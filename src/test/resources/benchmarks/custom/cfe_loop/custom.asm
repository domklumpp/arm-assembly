
file.elf:     file format elf32-littlearm


Disassembly of section .text:

00000000 <main>:
    0000:  00000000  mov r1, r2          ; save initial value of counter
    0004:  00000000  mov r3, r3, lsr #31 ; set target to 0 or 1
    0008:  00000000  cmp r2, #0          ; loop test
    000c:  00000000  beq 0020            ; exit loop if r2 is equal
    0010:  00000000  cmp r1, r2          ; compare counter to initial
    0014:  00000000  movne r3, #0        ; if distinct, set target to 0
    0018:  00000000  add r2, r2, #1      ; increment counter
    001c:  00000000  b 0008              ; re-enter loop
    0020:  00000000  mov r3, r3, lsl #2  ; multiply target by 4
    0024:  00000000  add r3, r3, #64     ; add 0040 to target
    0028:  00000000  bx r3               ; jump to target

    0040:  00000000  b 0040
    0044:  00000000  b 0044

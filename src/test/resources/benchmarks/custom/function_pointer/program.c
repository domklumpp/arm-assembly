
int f1(int c) {
  return c + 1;
}

int f2(int c) {
  return c + 2;
}

int f(int (*callback)(int)) {
  return callback(7);
}

void main() {
  int a = f(&f1);
  int b = f(&f2);
}

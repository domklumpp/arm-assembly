int x;

int main() {
  int y = 0;
  while (x > 0) {
    x--;
    if (y == 0)
      y = 1;
    else
      y = 0;
  }

  int z;
  switch (y) {
    case 0:
      z = 0;
      break;
    case 1:
      z = 1;
      break;
    case 2:
      z = -1;
      break;
    case 3:
      z = -1;
      break;
    case 4:
      z = -1;
      break;
    default:
      z = -1;
      break;
  }
}

int x;

int main() {
  int y;
  switch (x) {
    case 0:
      y = 1;
      break;
    case 1:
      y = 2;
      break;
    case 2:
      y = 4;
      break;
    case 3:
      y = 8;
      break;
    case 4:
      y = 16;
      break;
    default:
      y = -1;
      break;
  }
  return y;
}

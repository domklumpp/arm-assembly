package au.edu.mq.comp.assembly.tests.arm

import au.edu.mq.comp.assembly.InstructionSet
import au.edu.mq.comp.assembly.arm.AArch32
import au.edu.mq.comp.assembly.arm.controlflow.SuccessorAnalysis
import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax._
import au.edu.mq.comp.assembly.arm.syntax.PC
import au.edu.mq.comp.assembly.controlflow.UniqueSuccessorAnalysis.{SingleSuccessor, Unknown}
import au.edu.mq.comp.assembly.smt.{FreshSolver, SolverContext, Z3Simplifier}

class SuccessorAnalysisTest extends TestSpec {
  private implicit val baseInstructionSet: InstructionSet[Instruction] = AArch32
  private implicit val smtCtx: SolverContext = FreshSolver(solver("Z3"))

  private val successorResults = Table(
    ("name",                             "instruction",                                                                   "simplify", "hasUnique", "successor"),
    ("NOP",                              NopInstruction(NopMnemonic(AL())),                                                     false, Some(true),  SingleSuccessor(4)),
    ("ADD",                              DataInstruction(DataMnemonic(ADD(), SFlag(), AL()), R1(), R2(), ConstantOperand(4)),   false, Some(true),  SingleSuccessor(4)),
    ("ADDEQ",                            DataInstruction(DataMnemonic(ADD(), SFlag(), EQ()), R1(), R2(), ConstantOperand(4)),   true,  Some(true),  SingleSuccessor(4)),
    ("ADD to PC",                        DataInstruction(DataMnemonic(ADD(), NoSFlag(), AL()), PC(), PC(), ConstantOperand(4)), false, Some(true),  SingleSuccessor(12)),
    ("ADDEQ to PC",                      DataInstruction(DataMnemonic(ADD(), NoSFlag(), EQ()), PC(), PC(), ConstantOperand(4)), true,  None,        Unknown),
    ("ADD and store in PC",              DataInstruction(DataMnemonic(ADD(), NoSFlag(), AL()), PC(), R1(), ConstantOperand(4)), true,  None,        Unknown),
    ("ADD with PC and store in R1",      DataInstruction(DataMnemonic(ADD(), NoSFlag(), AL()), R1(), PC(), ConstantOperand(4)), false, Some(true),  SingleSuccessor(4)),
    ("B has single successor",           StaticJumpInstruction(B(AL()), 24, None),                                              false, Some(true),  SingleSuccessor(24)),
    ("BEQ may have multiple successors", StaticJumpInstruction(B(EQ()), 24, None),                                              true,  None,        Unknown)
  )

  private val unsimplifiedAnalysis = SuccessorAnalysis[Instruction]()
  private val simplifiedAnalysis = SuccessorAnalysis[Instruction](simplifier = Z3Simplifier)

  property("correctly detects single and multiple successor instructions") {
    forEvery(successorResults) { (_, instruction, simplify, expected, _) =>
      val actual = (if (simplify) simplifiedAnalysis else unsimplifiedAnalysis).hasUniqueSuccessor(instruction)
      actual should be (expected)
    }
  }

  property("correctly computes successor location") {
    forEvery(successorResults) { (_, instruction, simplify, _, expected) =>
      val actual = (if (simplify) simplifiedAnalysis else unsimplifiedAnalysis).computeUniqueSuccessor(0, instruction)
      actual should be (expected)
    }
  }
}

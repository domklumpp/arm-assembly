package au.edu.mq.comp.assembly.tests.arm

import au.edu.mq.comp.assembly.Program
import au.edu.mq.comp.assembly.arm.AArch32
import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax.Instruction

import org.bitbucket.franck44.scalasmt.configurations.{AppConfig, SolverConfig}
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{Matchers, PropSpec}

import java.io.File

abstract class TestSpec extends PropSpec with TableDrivenPropertyChecks with Matchers {
  protected lazy val benchmarks: Map[String, Seq[File]] = Set(
    "custom",
    "malardalen"
  )
    .map { set => (set, getBenchmarkFiles(set)) }
    .toMap

  private def getBenchmarkFiles(set: String): Seq[File] =
    listARMFiles(new File(getClass.getResource(s"/benchmarks/$set").getFile))
      .sortBy(_.getAbsolutePath)

  protected lazy val benchmarkFiles = Table("file",
    benchmarks.values.flatten
      .toSeq
      .sortBy(_.getAbsolutePath): _*
  )

  private def listARMFiles(file: File): Seq[File] = {
    if (file.isFile && file.getName.toLowerCase.endsWith(".asm"))
      List(file)
    else if (file.isDirectory)
      file.listFiles.flatMap(listARMFiles)
    else
      Nil
  }

  protected def parseProgram(file: String): Program[Instruction] = AArch32.buildProgram(new File(file)).get

  def solver(name: String): SolverConfig = AppConfig.config.find(_.name == name).get
}

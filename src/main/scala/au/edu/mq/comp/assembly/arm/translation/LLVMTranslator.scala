package au.edu.mq.comp.assembly
package arm.translation

import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax.Instruction
import au.edu.mq.comp.assembly.controlflow.{BasicBlock, BasicBlocks, ControlFlowGraph}

import org.scalallvm.assembly.AssemblySyntax._

final case class LLVMTranslator(cfg: ControlFlowGraph[Instruction, Guarded[Instruction]]) {
  def translate: Program = {
    val basicBlocks = BasicBlocks(cfg)
    val translationMap = basicBlocks.blocks.map { b => (b, translateBlock(b)) }.toMap

    // translate each block body
    // maintain list of occupied SSA names
    // for each block and each variable, maintain map from variable to input name, output name
    // add phi instruction at beginning of each block, translating from predecessor output names to block input names

    Program(Vector(createMainFunction(???, translationMap.values.toSet)))
  }

  private def createMainFunction(initialBlock: Block, otherBlocks: Set[Block]): FunctionDefinition =
    FunctionDefinition(
      DefaultLinkage(),
      NoPreemptionSpecifier(),
      DefaultVisibility(),
      DefaultDLLStorageClass(),
      DefaultCC(),
      Vector(),
      VoidT(),
      Global("main"),
      Args(Vector()),
      LocalUnnamedAddr(),
      Vector(Group("#0")),
      DefaultSection(),
      NoComdat(),
      DefaultAlignFunc(),
      NoGCName(),
      NoPrefix(),
      NoPrologue(),
      NoPersonality(),Vector(),
      FunctionBody(initialBlock +: otherBlocks.toVector)
    )

  // add init block that non-deterministically chooses entry block (if multiple), sets vars to nondeterministic values
  private def createEntryBlock(initialBlocks: Set[BasicBlock[cfg.State, Guarded[Instruction]]]): Block = ???

  private def translateBlock(block: BasicBlock[cfg.State, Guarded[Instruction]]): Block = {

    // TODO: use kiama attributes to compute in, out variables for each instruction, and for each block

    ???
  }
}

package au.edu.mq.comp.assembly
package smt

import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._
import org.bitbucket.franck44.scalasmt.theories._
import org.bitbucket.franck44.scalasmt.typedterms.{QuantifiedTerm, TypedTerm, VarTerm}

import scala.collection.IterableLike

object Syntax extends Core with BitVectors with ArrayExBV with ArrayExOperators with QuantifiedTerm {

  def exists(vars: Set[VarTerm[_]])(formula: => Formula): Formula = {
    if (vars.isEmpty)
      formula
    else {
      val symbols = vars.map(_.symbol).toSeq
      exists(symbols.head, symbols.tail: _*)(formula)
    }
  }

  def forall(vars: Set[SortedQId])(formula: Formula): Formula = {
    if (vars.isEmpty)
      formula
    else {
      TypedTerm[BoolTerm, ForAllTerm](
        formula.typeDefs -- vars,
        ForAllTerm(
          vars.map { case SortedQId(SymbolId(symbol), sort) => SortedVar(symbol, sort) }.toList,
          formula.termDef))
    }
  }

  def let[T](subst: TypedMap[VarTerm, Expr])(expr: Expr[T]): Expr[T] = {
    val freeVars = FreeVariables.compute(expr)

    val (bindings, boundDefs, substDefs) = subst
      .filter(isRelevant(freeVars)) // no substitution for variables that do not occur freely
      .map(extractDefs).unzip3

    if (bindings.isEmpty)
      expr
    else
      TypedTerm[T, LetTerm](
        (expr.typeDefs -- boundDefs) ++ substDefs.flatten,
        LetTerm(bindings.toList, expr.termDef)
      )
  }

  private case class isRelevant(free: Set[QualifiedId]) extends TypedMap.Callback[VarTerm, Expr, Boolean] {
    override def apply[T](k: VarTerm[T], v: Expr[T]): Boolean = {
      val (simple, sorted) = k.aTerm.qualifiedId match {
        case s@SimpleQId(id)    => (s, SortedQId(id, k.sort))
        case s@SortedQId(id, _) => (SimpleQId(id), s)
      }
      free.contains(simple) || free.contains(sorted)
    }
  }

  private object extractDefs extends TypedMap.Callback[VarTerm, Expr, (VarBinding, SortedQId, Set[SortedQId])] {
    override def apply[T](k: VarTerm[T], v: Expr[T]): (VarBinding, SortedQId, Set[SortedQId]) = {
      val id = k.aTerm.qualifiedId match {
        case SimpleQId(i)      => SortedQId(i, k.sort)
        case s@SortedQId(_, _) => s
      }
      (VarBinding(k.symbol, v.termDef), id, v.typeDefs)
    }
  }

  implicit class FormulaSetExtensions[Repr](coll: IterableLike[Formula, Repr]) {
    def conjunction: Formula = if (coll.isEmpty) True() else coll.reduce(_ && _)

    def disjunction: Formula = if (coll.isEmpty) False() else coll.reduce(_ || _)
  }

  implicit class FormulaExtensions(formula: Formula) {
    def &&(other: Formula): Formula = {
      if (formula == False() || other == False())
        False()
      else if (formula == True())
        other
      else if (other == True())
        formula
      else
        formula & other
    }

    def ||(other: Formula): Formula = {
      if (formula == True() || other == True())
        True()
      else if (formula == False())
        other
      else if (other == False())
        formula
      else
        formula | other
    }

    def implies(other: Formula): Formula = {
      if (formula == False() || other == True())
        True()
      else if (formula == True())
        other
      else if (other == False())
        !formula
      else
        formula imply other
    }

    def ifThenElse[T](thenExpr: Expr[T], elseExpr: Expr[T]): Expr[T] = {
      if (formula == True())
        thenExpr
      else if (formula == False())
        elseExpr
      else
        formula.ite(thenExpr, elseExpr)
    }
  }

  object AnyQId {
    def unapply(qid: QualifiedId): Option[Id] = qid match {
      case SimpleQId(id)    => Some(id)
      case SortedQId(id, _) => Some(id)
    }
  }

}

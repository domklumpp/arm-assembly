package au.edu.mq.comp.assembly
package smt

import au.edu.mq.comp.assembly.smt.Syntax._

/**
  * Encodes instruction traces in SMT formulas.
  */
object SmtEncoder {

  /**
    * Encodes the given trace.
    *
    * @param trace A sequence of instructions.
    * @return A sequence of formulas, one for each instruction in the trace, and a sequence of states (one more than there are instructions).
    *
    * @tparam I The type of instructions.
    */
  def encodeTrace[I: InstructionSet](trace: Seq[I]): (List[Formula], List[VariableMap]) = {
    val (formulas, states) = encodeTraceFormulas(trace)
    (formulas.map { case (pre, upd) => (pre ++ upd).conjunction }, states)
  }

  def encodeTraceFormulas[I: InstructionSet](trace: Seq[I]): (List[(Set[Formula], Set[Formula])], List[VariableMap]) = {
    val initialMap = VariableMap.initial
    val (_, formulas, states) = trace.foldLeft((initialMap, Nil: List[(Set[Formula], Set[Formula])], List(initialMap))) {
      case ((map, fmas, sts), i) =>
        val (pre, upd, updatedMap) = encodeInstructionFormulas(i, map)
        (updatedMap, fmas :+ (pre, upd), sts :+ updatedMap)
    }

    (formulas, states)
  }

  /**
    * Encodes the effect of the given instruction.
    *
    * @param instruction The instruction.
    * @param variableMap An initial map from program variables to logic variables.
    * @return A formula encoding the effect of the instruction, and a new map from program variables to logic variables representing values after the execution.
    */
  def encodeInstruction[I: InstructionSet](instruction: I, variableMap: VariableMap): (Formula, VariableMap) = {
    val (pre, upd, outMap) = encodeInstructionFormulas(instruction, variableMap)
    ((pre ++ upd).conjunction, outMap)
  }

  def encodeInstructionFormulas[I: InstructionSet](instruction: I, variableMap: VariableMap): (Set[Formula], Set[Formula], VariableMap) = {
    val instructionSemantics = instruction.semantics

    val definedFormulas = instructionSemantics.isDefined.map(variableMap.substitute)
    val stateUpdate = instructionSemantics.update

    val outMap = stateUpdate.keySet.foldLeft(variableMap)(_.bump(_))
    val updateFormulas = stateUpdate.map(buildEquation(variableMap, outMap))

    (definedFormulas, updateFormulas, outMap)
  }

  private def buildEquation(oldState: VariableMap, updatedState: VariableMap) = new TypedMap.Callback[ProgramVariable, Expr, Formula] {
    override def apply[T](v: ProgramVariable[T], e: Expr[T]): Formula = updatedState(v) === oldState.substitute(e)
  }
}
package au.edu.mq.comp.assembly.arm

import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax._

package object syntax {

  object PC {
    def apply(): R15 = R15()

    def unapply(r: Register): Boolean = r == R15()
  }

  object LR {
    def apply(): R14 = R14()

    def unapply(r: Register): Boolean = r == R14()
  }

  object SP {
    def apply(): R13 = R13()

    def unapply(r: Register): Boolean = r == R13()
  }

  object IP {
    def apply(): R12 = R12()

    def unapply(r: Register): Boolean = r == R12()
  }

  object FP {
    def apply(): R11 = R11()

    def unapply(r: Register): Boolean = r == R11()
  }

  object SL {
    def apply(): R10 = R10()

    def unapply(r: Register): Boolean = r == R10()
  }

  implicit class RegisterOp(r: Register) {
    /**
      * Converts a register to its index (0 - 15).
      */
    def registerIndex: Int = r match {
      case R15() => 15
      case R14() => 14
      case R13() => 13
      case R12() => 12
      case R11() => 11
      case R10() => 10
      case R9()  => 9
      case R8()  => 8
      case R7()  => 7
      case R6()  => 6
      case R5()  => 5
      case R4()  => 4
      case R3()  => 3
      case R2()  => 2
      case R1()  => 1
      case R0()  => 0
    }
  }

  /**
    * Given an index from 0 to 15 (inclusive), returns the corresponding register.
    */
  def register(index: Int): Register = {
    require(0 <= index && index <= 15, s"Invalid register index $index.")
    index match {
      case 15 => R15()
      case 14 => R14()
      case 13 => R13()
      case 12 => R12()
      case 11 => R11()
      case 10 => R10()
      case 9  => R9()
      case 8  => R8()
      case 7  => R7()
      case 6  => R6()
      case 5  => R5()
      case 4  => R4()
      case 3  => R3()
      case 2  => R2()
      case 1  => R1()
      case 0  => R0()
    }
  }

}

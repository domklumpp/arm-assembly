package au.edu.mq.comp.assembly
package smt

import org.bitbucket.franck44.scalasmt.configurations.SMTLogics.SMTLogics

/**
  * A common trait for different methods of expression simplification.
  */
trait Simplifier {
  /**
    * Converts an expression into a simpler expression that is guaranteed to have the same value.
    *
    * @param expr    The expression to simplify.
    * @param logic   The SMT logic in which expr and context are expressed.
    * @param context A set of logical formulas whose validity may be assumed when simplifying (optional).
    * @tparam T The logical type of the original and the simplified expression.
    * @return An expression e satisfying ( |= context => expr = e ). This method must not fail: if inapplicable, simply returns the original expression.
    */
  def simplify[T](expr: Expr[T], logic: SMTLogics, context: Set[Formula] = Set.empty): Expr[T]
}

/**
  * A simple implementation that does no simplification whatsoever.
  */
object NoSimplifier extends Simplifier {
  override def simplify[T](expr: Expr[T], logic: SMTLogics, context: Set[Formula] = Set.empty): Expr[T] = expr
}

/**
  * Uses the Z3 "simplify" command to simplify expressions.
  */
object Z3Simplifier extends Simplifier {

  import au.edu.mq.comp.assembly.smt.Solving._

  import org.bitbucket.franck44.scalasmt.configurations.SMTInit
  import org.bitbucket.franck44.scalasmt.configurations.SMTLogics.SMTLogics
  import org.bitbucket.franck44.scalasmt.interpreters.SMTSolver
  import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Parser
  import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._
  import org.bitbucket.franck44.scalasmt.typedterms.TypedTerm
  import org.bitbucket.inkytonik.kiama.util.StringSource

  import scala.util.Try

  override def simplify[T](expr: Expr[T], logic: SMTLogics, context: Set[Formula] = Set.empty): Expr[T] = {
    val responseParser = SMTLIB2Parser[Term]

    val result = using(new SMTSolver("Z3", SMTInit(Some(logic), Nil))) { implicit solver =>
      for {
        _ <- expr.typeDefs.tryMapAll(declare)
        response <- solver.eval(Raw(s"(simplify ${expr.show})"))
        parsed <- responseParser(StringSource(response.trim))
        normalized = Z3Normalizer.normalize(parsed)
      } yield new TypedTerm[T, Term](expr.typeDefs, normalized) // TODO: properly retrieve necessary declarations only
    }
    result.getOrElse(expr)
  }

  private def declare(variable: SortedQId)(implicit solver: SMTSolver): Try[_] = variable match {
    // TODO: incomplete match
    case SortedQId(SymbolId(id), sort) =>
      val command = DeclareFunCmd(FunDecl(id, List(), sort))
      val responseParser = SMTLIB2Parser[SuccessResponse]
      for {
        response <- solver.eval(command)
        parsed <- responseParser(StringSource(response))
      } yield parsed
  }
}
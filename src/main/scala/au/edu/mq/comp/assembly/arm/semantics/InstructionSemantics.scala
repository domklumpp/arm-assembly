package au.edu.mq.comp.assembly
package arm.semantics

import au.edu.mq.comp.assembly.arm.ConditionFlag._
import au.edu.mq.comp.assembly.arm.semantics.IntegerSupport.{SInt, UInt}
import au.edu.mq.comp.assembly.arm.semantics.Library._
import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax._
import au.edu.mq.comp.assembly.arm.syntax.{LR, PC, SP, register}
import au.edu.mq.comp.assembly.semantics._
import au.edu.mq.comp.assembly.smt.Syntax._

object InstructionSemantics extends SemanticsDefinition[Instruction] with Logging {

  // Taken from the ARM Architecture Reference Manual (ARMv8, for ARMv8-A architecture profile) version B.a.
  // <https://static.docs.arm.com/ddi0487/b/DDI0487B_a_armv8_arm.pdf>
  // Page numbers in comments refer to this document.

  implicit val wordSize32: WordSize = WordSize.Bits32

  // Signed bitvectors are used for immediate constants in place of any call to A32ExpandImm_C, A32ExpandImm in the pseudo-code.
  // Otherwise, unsigned bitvectors are used.

  override def apply(i: Instruction): Semantics = {
    i match {
      // MLA, MLAS -- p. 3254
      case AccumulateInstruction(AccumulateMnemonic(MLA(), sFlag, _), rd, rn, rm, ra) if !Set(rd, rn, rm, ra).contains(PC()) =>
        val result = SInt(rn) * SInt(rm) + SInt(ra)
        (rd.asVar |-> result.extract(31, 0)) ++ setFlags(sFlag, result.bv(31), isZero(result.extract(31, 0)), C, V)

      // LDM, LDMIA, LDMFD -- p. 3144
      case BatchMemoryInstruction(BatchMemoryMnemonic(LDM(), None | Some(IncrementAfter() | FullDescending()), _), rn, upd, registers)
        if rn != PC() && registers.nonEmpty && !(registers.contains(rn) && upd == Update()) =>

        val wback = upd == Update()
        val address = rn.asVar
        val reg = registers.filterNot(_ == PC()).distinct.sortBy(_.registerIndex).zipWithIndex

        for {
          // read memory into registers
          regUpdate <- reg.foldLeftM(noUpdate) {
            case (stateUpdate, (ri, k)) => for (data <- stateUpdate |> readMemA(address + (4 * k).bvu, 4)) yield stateUpdate ++ (ri.asVar |-> data)
          }

          // read memory into PC (if included)
          pcData <- readMemA(address + (4 * (registers.size - 1)).bvu, 4)
          pcUpdate <- if (registers.contains(PC())) loadWritePC(pcData) else Guarded(noUpdate)

          // update rn register (if wback enabled)
          rnUpdate = if (wback) rn.asVar |-> rn.asVar + (4 * registers.size).bvu else noUpdate

        } yield regUpdate ++ pcUpdate ++ rnUpdate

      // LDMDB, LDMEA -- p. 3154
      case BatchMemoryInstruction(BatchMemoryMnemonic(LDM(), None | Some(DecrementBefore() | EmptyAscending()), _), rn, upd, registers)
        if rn != PC() && registers.nonEmpty && !(registers.contains(rn) && upd == Update()) =>

        val wback = upd == Update()
        val address = rn.asVar - (4 * registers.size).bvu
        val reg = registers.filterNot(_ == PC()).distinct.sortBy(_.registerIndex).zipWithIndex

        for {
          // read memory into registers
          regUpdate <- reg.foldLeftM(noUpdate) {
            case (stateUpdate, (ri, k)) => for (data <- stateUpdate |> readMemA(address + (4 * k).bvu, 4)) yield stateUpdate ++ (ri.asVar |-> data)
          }

          // read memory into PC (if included)
          pcData <- readMemA(address + (4 * (registers.size - 1)).bvu, 4)
          pcUpdate <- if (registers.contains(PC())) loadWritePC(pcData) else Guarded(noUpdate)

          // update rn register (if wback enabled)
          rnUpdate = if (wback) rn.asVar |-> rn.asVar - (4 * registers.size).bvu else noUpdate

        } yield regUpdate ++ pcUpdate ++ rnUpdate

      // LDMIB, LDMED -- p. 3157
      case BatchMemoryInstruction(BatchMemoryMnemonic(LDM(), Some(IncrementBefore() | EmptyDescending()), _), rn, upd, registers)
        if rn != PC() && registers.nonEmpty && !(registers.contains(rn) && upd == Update()) =>

        val wback = upd == Update()
        val address = rn.asVar + 4.bvu
        val reg = registers.filterNot(_ == PC()).distinct.sortBy(_.registerIndex).zipWithIndex

        for {
          // read memory into registers
          regUpdate <- reg.foldLeftM(noUpdate) {
            case (stateUpdate, (ri, k)) => for (data <- stateUpdate |> readMemA(address + (4 * k).bvu, 4)) yield stateUpdate ++ (ri.asVar |-> data)
          }

          // read memory into PC (if included)
          pcData <- readMemA(address + (4 * reg.size).bvu, 4)
          pcUpdate <- if (registers.contains(PC())) loadWritePC(pcData) else Guarded(noUpdate)

          // update rn register (if wback enabled)
          rnUpdate = if (wback) rn.asVar |-> rn.asVar - (4 * registers.size).bvu else noUpdate

        } yield regUpdate ++ pcUpdate ++ rnUpdate


      // STM, STMIA, STMEA -- p. 3495
      case BatchMemoryInstruction(BatchMemoryMnemonic(STM(), None | Some(IncrementAfter() | EmptyAscending()), _), rn, upd, registers)
        if rn != PC() && registers.nonEmpty && !(registers.contains(rn) && upd == Update() && rn != registers.minBy(_.registerIndex)) =>

        val wback = upd == Update()
        val address = rn.asVar
        val reg = registers.distinct.sortBy(_.registerIndex).zipWithIndex

        for {
          update <- reg.foldLeftM(noUpdate) {
            case (stateUpdate, (ri, k)) => stateUpdate +>> writeMemA(address + (4 * k).bvu, 4, ri)
          }
          writeBack = if (wback) rn.asVar |-> address + (4 * registers.size).bvu else noUpdate
        } yield update ++ writeBack


      // STMDB, STMFD -- p. 3503
      case BatchMemoryInstruction(BatchMemoryMnemonic(STM(), Some(DecrementBefore() | FullDescending()), _), rn, upd, registers)
        if rn != PC() && registers.nonEmpty && !(registers.contains(rn) && upd == Update() && rn != registers.minBy(_.registerIndex)) =>

        val wback = upd == Update()
        val address = rn.asVar - (4 * registers.size).bvu
        val reg = registers.distinct.sortBy(_.registerIndex).zipWithIndex

        for {
          update <- reg.foldLeftM(noUpdate) {
            case (stateUpdate, (ri, k)) => stateUpdate +>> writeMemA(address + (4 * k).bvu, 4, ri)
          }
          writeBack = if (wback) rn.asVar |-> address else noUpdate
        } yield update ++ writeBack

      // STMIB, STMFA -- p. 3506
      case BatchMemoryInstruction(BatchMemoryMnemonic(STM(), Some(IncrementBefore() | FullAscending()), _), rn, upd, registers)
        if rn != PC() && registers.nonEmpty && !(registers.contains(rn) && upd == Update() && rn != registers.minBy(_.registerIndex)) =>

        val wback = upd == Update()
        val address = rn.asVar + 4.bvu
        val reg = registers.distinct.sortBy(_.registerIndex).zipWithIndex

        for {
          update <- reg.foldLeftM(noUpdate) {
            case (stateUpdate, (ri, k)) => stateUpdate +>> writeMemA(address + (4 * k).bvu, 4, ri)
          }
          writeBack = if (wback) rn.asVar |-> rn.asVar + (4 * reg.size).bvu else noUpdate
        } yield update ++ writeBack

      // CLZ -- p. 3076
      case CountBitInstruction(CountBitMnemonic(CLZ(), _), rd, rm) if rd != PC() && rm != PC() =>
        rd.asVar |-> countLeadingZeroBits(rm).extract(31, 0)

      // CMN (immediate) -- p. 3078
      case ComparisonInstruction(ComparisonMnemonic(CMN(), _), rn, ConstantOperand(imm)) =>
        val (_, n, z, c, v) = addWithCarry(rn, imm.bvs, carryIn = false)
        setFlags(SFlag(), n, z, c, v)

      // CMP (immediate) -- p. 3083
      case ComparisonInstruction(ComparisonMnemonic(CMP(), _), rn, ConstantOperand(imm)) =>
        val (_, n, z, c, v) = addWithCarry(rn, !imm.bvs, carryIn = true)
        setFlags(SFlag(), n, z, c, v)

      // CMP (register) -- p. 3085
      case ComparisonInstruction(ComparisonMnemonic(CMP(), _), rn, SimpleRegisterShift(rm, shift_t, shift_n)) =>
        val shifted = shift(rm, shift_t, shift_n, C)
        val (_, n, z, c, v) = addWithCarry(rn, !shifted, carryIn = true)
        setFlags(SFlag(), n, z, c, v)

      // CMP (register-shifted register) -- p. 3088
      case ComparisonInstruction(ComparisonMnemonic(CMP(), _), rn, RegisterShiftReg(rm, shift_t, rs)) if rn != PC() && rm != PC() && rs != PC() =>
        val shift_n = UInt(rs.asVar.extract(7, 0))(WordSize.Byte)
        val shifted = shift(rm, toSRType(shift_t), shift_n, C)
        val (_, n, z, c, v) = addWithCarry(rn, !shifted, true)
        setFlags(SFlag(), n, z, c, v)

      // TEQ (immediate) -- p. 3590
      case ComparisonInstruction(ComparisonMnemonic(TEQ(), _), rn, ConstantOperand(imm)) =>
        val result = rn.asVar xor imm.bvs
        setFlags(SFlag(), result(31), isZero(result), extractCarry(imm), V)

      // TEQ (register) -- p. 3592
      case ComparisonInstruction(ComparisonMnemonic(TEQ(), _), rn, SimpleRegisterShift(rm, shift_t, shift_n)) =>
        val (shifted, carry) = shiftC(rm, shift_t, shift_n, C)
        val result = rn.asVar xor shifted
        setFlags(SFlag(), result(31), isZero(result), carry, V)

      // TST (immediate) -- p. 3595
      case ComparisonInstruction(ComparisonMnemonic(TST(), _), rn, ConstantOperand(imm)) =>
        val result = rn.asVar and imm.bvs
        setFlags(SFlag(), result(31), isZero(result), extractCarry(imm), V)

      // TST (register) -- p. 3597
      case ComparisonInstruction(ComparisonMnemonic(TST(), _), rn, SimpleRegisterShift(rm, shift_t, shift_n)) =>
        val (shifted, carry) = shiftC(rm, shift_t, shift_n, C)
        val result = rn.asVar and shifted
        setFlags(SFlag(), result(31), isZero(result), carry, V)

      // ADC, ADCS (immediate) -- p. 3005
      case DataInstruction(DataMnemonic(ADC(), sFlag, _), rd, rn, ConstantOperand(imm)) if rd != PC() || sFlag == NoSFlag() =>
        val (result, n, z, c, v) = addWithCarry(rn, imm.bvs, C)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // ADC, ADCS (register) -- p. 3007
      case DataInstruction(DataMnemonic(ADC(), sFlag, _), rd, rn, SimpleRegisterShift(rm, shift_t, shift_n)) if rd != PC() || sFlag == NoSFlag() =>
        val shifted = shift(rm, shift_t, shift_n, C)
        val (result, n, z, c, v) = addWithCarry(rn, shifted, C)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // ADC, ADCS (register-shifted register) -- p. 3007
      case DataInstruction(DataMnemonic(ADC(), sFlag, _), rd, rn, RegisterShiftReg(rm, shift_t, rs)) if !Set(rd, rn, rm, rs).contains(PC()) =>
        val shift_n = UInt(rn.asVar.extract(7, 0))(WordSize.Byte)
        val shifted = shift(rm, toSRType(shift_t), shift_n, C)
        val (result, n, z, c, v) = addWithCarry(rn, shifted, C)
        (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // ADD, ADDS (immediate); ADD, ADDS (SP plus immediate) -- p. 3013; p. 3023
      case DataInstruction(DataMnemonic(ADD(), sFlag, _), rd, rn, ConstantOperand(imm)) if rd != PC() || sFlag == NoSFlag() =>
        val (result, n, z, c, v) = addWithCarry(rn, imm.bvs, carryIn = false)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // ADD, ADDS (register) -- p. 3017
      case DataInstruction(DataMnemonic(ADD(), sFlag, _), rd, rn, SimpleRegisterShift(rm, shift_t, shift_n)) if rd != PC() || sFlag == NoSFlag() =>
        val shifted = shift(rm, shift_t, shift_n, C)
        val (result, n, z, c, v) = addWithCarry(rn, shifted, carryIn = false)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // ADD, ADDS (register-shifted register) -- p. 3021
      case DataInstruction(DataMnemonic(ADD(), sFlag, _), rd, rn, RegisterShiftReg(rm, shift_t, rs)) if rd != PC() && rn != PC() && rm != PC() && rs != PC() =>
        val shift_n = UInt(rs.asVar.extract(7, 0))(WordSize.Byte)
        val shifted = shift(rm, toSRType(shift_t), shift_n, C)
        val (result, n, z, c, v) = addWithCarry(rn, shifted, false)
        (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // AND, ANDS (immediate) -- p. 3035
      case DataInstruction(DataMnemonic(AND(), sFlag, _), rd, rn, ConstantOperand(imm)) if rd != PC() || sFlag == NoSFlag() =>
        val result = rn.asVar and imm.bvs
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), extractCarry(imm), V)

      // AND, ANDS (register) -- p. 3037
      case DataInstruction(DataMnemonic(AND(), sFlag, _), rd, rn, SimpleRegisterShift(rm, shift_t, shift_n)) if rd != PC() || sFlag == NoSFlag() =>
        val (shifted, carry) = shiftC(rm, shift_t, shift_n, C)
        val result = rn.asVar and shifted
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), carry, V)

      // BIC, BICS (immediate) -- p. 3058
      case DataInstruction(DataMnemonic(BIC(), sFlag, _), rd, rn, ConstantOperand(imm)) if rd != PC() || sFlag == NoSFlag() =>
        val result = rn.asVar and !imm.bvs
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), extractCarry(imm), V)

      // BIC, BICS (register) -- p. 3060
      case DataInstruction(DataMnemonic(BIC(), sFlag, _), rd, rn, SimpleRegisterShift(rm, shift_t, shift_n)) if rd != PC() || sFlag == NoSFlag() =>
        val (shifted, carry) = shiftC(rm, shift_t, shift_n, C)
        val result = rn.asVar and !shifted
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), carry, V)

      // BIC, BICS (register-shifted register) -- p. 3063
      case DataInstruction(DataMnemonic(BIC(), sFlag, _), rd, rn, RegisterShiftReg(rm, shift_t, rs)) if !Set(rd, rn, rm, rs).contains(PC()) =>
        val shift_n = UInt(rs.asVar.extract(7, 0))(WordSize.Byte)
        val (shifted, carry) = shiftC(rm, toSRType(shift_t), shift_n, C)
        val result = rn.asVar and !shifted
        (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), carry, V)

      // EOR, EORS (immediate) -- p. 3109
      case DataInstruction(DataMnemonic(EOR(), sFlag, _), rd, rn, ConstantOperand(imm)) if rd != PC() || sFlag == NoSFlag() =>
        val result = rn.asVar xor imm.bvs
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), extractCarry(imm), V)

      // EOR, EORS (register) -- p. 3111
      case DataInstruction(DataMnemonic(EOR(), sFlag, _), rd, rn, SimpleRegisterShift(rm, shift_t, shift_n)) if rd != PC() || sFlag == NoSFlag() =>
        val (shifted, carry) = shiftC(rm, shift_t, shift_n, C)
        val result = rn.asVar xor shifted
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), carry, V)

      // EOR, EORS (register-shifted register) -- p. 3115
      case DataInstruction(DataMnemonic(EOR(), sFlag, _), rd, rn, RegisterShiftReg(rm, shift_t, rs)) if !Set(rd, rn, rm, rs).contains(PC()) =>
        val shift_n = UInt(rs.asVar.extract(7, 0))(WordSize.Byte)
        val (shifted, carry) = shiftC(rm, toSRType(shift_t), shift_n, C)
        val result = rn.asVar xor shifted
        (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), carry, V)


      // MUL, MULS -- p. 3290
      case DataInstruction(DataMnemonic(MUL(), sFlag, _), rd, rn, RegisterOperand(rm)) if !Set(rd, rn, rm).contains(PC()) =>
        val result = SInt(rn) * SInt(rm)
        (rd.asVar |-> result.extract(31, 0)) ++ setFlags(sFlag, result.bv(31), isZero(result.extract(31, 0)), C, V)

      // ORR, ORRS (immediate) -- p. 3304
      case DataInstruction(DataMnemonic(ORR(), sFlag, _), rd, rn, ConstantOperand(imm)) if rd != PC() || sFlag == NoSFlag() =>
        val result = rn.asVar or imm.bvs
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), extractCarry(imm), V)

      // ORR, ORRS (register) -- p. 3306
      case DataInstruction(DataMnemonic(ORR(), sFlag, _), rd, rn, SimpleRegisterShift(rm, shift_t, shift_n)) if rd != PC() || sFlag == NoSFlag() =>
        val (shifted, carry) = shiftC(rm, shift_t, shift_n, C)
        val result = rn.asVar or shifted
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), carry, V)

      // ORR, ORRS (register-shifted register) -- p. 3310
      case DataInstruction(DataMnemonic(ORR(), sFlag, _), rd, rn, RegisterShiftReg(rm, shift_t, rs)) if !Set(rd, rn, rm, rs).contains(PC()) =>
        val shift_n = UInt(rs.asVar.extract(7, 0))(WordSize.Byte)
        val (shifted, carry) = shiftC(rm, toSRType(shift_t), shift_n, C)
        val result = rn.asVar or shifted
        (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), carry, V)

      // RSB, RSBS (immediate) -- p. 3378
      case DataInstruction(DataMnemonic(RSB(), sFlag, _), rd, rn, ConstantOperand(imm)) if rd != PC() || sFlag == NoSFlag() =>
        val (result, n, z, c, v) = addWithCarry(!rn.asVar, imm.bvs, carryIn = true)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // RSB, RSBS (register) -- p. 3381
      case DataInstruction(DataMnemonic(RSB(), sFlag, _), rd, rn, SimpleRegisterShift(rm, shift_t, shift_n)) if rd != PC() || sFlag == NoSFlag() =>
        val shifted = shift(rm, shift_t, shift_n, C)
        val (result, n, z, c, v) = addWithCarry(!rn.asVar, shifted, carryIn = true)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // RSC, RSCS (immediate) -- p. 3386
      case DataInstruction(DataMnemonic(RSC(), sFlag, _), rd, rn, ConstantOperand(imm)) if rd != PC() || sFlag == NoSFlag() =>
        val (result, n, z, c, v) = addWithCarry(!rn.asVar, imm.bvs, C)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // RSC, RSCS (register) -- p. 3388
      case DataInstruction(DataMnemonic(RSC(), sFlag, _), rd, rn, SimpleRegisterShift(rm, shift_t, shift_n)) if rd != PC() || sFlag == NoSFlag() =>
        val shifted = shift(rm, shift_t, shift_n, C)
        val (result, n, z, c, v) = addWithCarry(!rn.asVar, shifted, C)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // SBC, SBCS (immediate) -- p. 3398
      case DataInstruction(DataMnemonic(SBC(), sFlag, _), rd, rn, ConstantOperand(imm)) if rd != PC() || sFlag == NoSFlag() =>
        val (result, n, z, c, v) = addWithCarry(rn, !imm.bvs, C)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // SBC, SBCS (register) -- p. 3400
      case DataInstruction(DataMnemonic(SBC(), sFlag, _), rd, rn, SimpleRegisterShift(rm, shift_t, shift_n)) if rd != PC() || sFlag == NoSFlag() =>
        val shifted = shift(rm, shift_t, shift_n, C)
        val (result, n, z, c, v) = addWithCarry(rn, !shifted, C)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // SUB, SUBS (immediate) -- p. 3560
      case DataInstruction(DataMnemonic(SUB(), sFlag, _), rd, rn, ConstantOperand(imm)) if rn != SP() && (rd != PC() || sFlag == NoSFlag()) =>
        val (result, n, z, c, v) = addWithCarry(rn, !imm.bvs, carryIn = true)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // SUB, SUBS (SP minus immediate) -- p. 3569
      case DataInstruction(DataMnemonic(SUB(), sFlag, _), rd, SP(), ConstantOperand(imm)) if rd != PC() || sFlag == NoSFlag() =>
        val (result, n, z, c, v) = addWithCarry(sp, !imm.bvs, carryIn = true)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // SUB, SUBS (register) -- p. 3564
      case DataInstruction(DataMnemonic(SUB(), sFlag, _), rd, rn, SimpleRegisterShift(rm, shift_t, shift_n)) if rd != PC() || sFlag == NoSFlag() =>
        val shifted = shift(rm, shift_t, shift_n, C)
        val (result, n, z, c, v) = addWithCarry(rn, !shifted, carryIn = true)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // SUB, SUBS (register-shifted register) -- p. 3567
      case DataInstruction(DataMnemonic(SUB(), sFlag, _), rd, rn, RegisterShiftReg(rm, shift_t, rs)) if !Set(rd, rn, rm, rs).contains(PC()) =>
        val shift_n = UInt(rs.asVar.extract(7, 0))(WordSize.Byte)
        val shifted = shift(rm, toSRType(shift_t), shift_n, C)
        val (result, n, z, c, v) = addWithCarry(rn, !shifted, carryIn = true)
        (rd.asVar |-> result) ++ setFlags(sFlag, n, z, c, v)

      // UMLAL, UMLALS -- p. 3626
      case LongInstruction(LongMnemonic(UMLAL(), sFlag, _), rdLo, rdHi, rn, rm) if !Set(rdLo, rdHi, rn, rm).contains(PC()) && rdLo != rdHi =>
        val result = UInt(rn) * UInt(rm) + UInt(rdHi.asVar.concat(rdLo))
        (rdHi.asVar |-> result.extract(63, 32)) ++
          (rdLo.asVar |-> result.extract(31, 0)) ++
          setFlags(sFlag, result.bv(63), isZero(result.extract(63, 0)), C, V)

      // UMULL, UMULLS -- p. 3628
      case LongInstruction(LongMnemonic(UMULL(), sFlag, _), rdLo, rdHi, rn, rm) if !Set(rdLo, rdHi, rn, rm).contains(PC()) && rdLo != rdHi =>
        val result = UInt(rn) * UInt(rm)
        (rdHi.asVar |-> result.extract(63, 32)) ++
          (rdLo.asVar |-> result.extract(31, 0)) ++
          setFlags(sFlag, result.bv(63), isZero(result.extract(63, 0)), C, V)

      // SMULL, SMULLS -- p. 3458
      case LongInstruction(LongMnemonic(SMULL(), sFlag, _), rdLo, rdHi, rn, rm) if !Set(rdLo, rdHi, rn, rm).contains(PC()) && rdLo != rdHi =>
        val result = SInt(rn) * SInt(rm)
        (rdHi.asVar |-> result.extract(63, 32)) ++
          (rdLo.asVar |-> result.extract(31, 0)) ++
          setFlags(sFlag, result.bv(63), isZero(result.extract(63, 0)), C, V)

      // LDR (immediate) -- p. 3159
      case MemoryInstruction(MemoryMnemonic(LDR(), Bit32(), _), rt, ImmediateMemoryReference(rn, imm, index, add, wback)) if rn != PC() && !(wback && rn == rt) =>
        val offset_addr = if (add) rn.asVar + imm.bvu else rn.asVar - imm.bvu
        val address = if (index) offset_addr else rn.asVar

        for {
          data <- readMemU(address, 4)
          if isAligned(address) || rt != PC()
          update <- if (rt == PC()) loadWritePC(data) else Guarded(rt.asVar |-> data)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield update ++ writeBack

      // LDR (literal) -- p. 3163
      case MemoryInstruction(MemoryMnemonic(LDR(), Bit32(), _), rt, ImmediateMemoryReference(PC(), imm, _, add, false)) =>
        val base = align(pc, 4)
        val address = if (add) base + imm.bvu else base - imm.bvu
        for {
          data <- readMemU(address, 4)
          if isAligned(address) || rt != PC()
          update <- if (rt == PC()) loadWritePC(data) else Guarded(rt.asVar |-> data)
        } yield update

      // LDR (register) -- p. 3166
      case MemoryInstruction(MemoryMnemonic(LDR(), Bit32(), _), rt, RegisterMemoryReference(rn, rm, index, add, wback, shift_t, shift_n)) if rm != PC() && !(wback && (rn == PC() || rn == rt)) =>
        val offset = shift(rm, shift_t, shift_n, C)
        val offset_addr = if (add) rn.asVar + offset else rn.asVar - offset
        val address = if (index) offset_addr else rn.asVar

        for {
          data <- readMemU(address, 4)
          if isAligned(address) || rt != PC()
          update <- if (rt == PC()) loadWritePC(data) else Guarded(rt.asVar |-> data)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield update ++ writeBack

      // LDRB (immediate) -- p. 3169
      case MemoryInstruction(MemoryMnemonic(LDR(), Bit8(), _), rt, ImmediateMemoryReference(rn, imm, index, add, wback)) if rn != PC() && !(rt == PC() || (wback && rn == rt)) =>
        val offset_addr = if (add) rn.asVar + imm.bvu else rn.asVar - imm.bvu
        val address = if (index) offset_addr else rn.asVar
        for {
          byte <- readMemU(address, 1)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield (rt.asVar |-> byte.zext(32 - 8)) ++ writeBack

      // LDRB (literal) -- p. 3172
      case MemoryInstruction(MemoryMnemonic(LDR(), Bit8(), _), rt, ImmediateMemoryReference(PC(), imm, _, add, false)) if rt != PC() =>
        val base = align(pc, 4)
        val address = if (add) base + imm.bvu else base - imm.bvu
        for {
          byte <- readMemU(address, 1)
        } yield rt.asVar |-> byte.zext(32 - 8)

      // LDRB (register) -- p. 3174
      case MemoryInstruction(MemoryMnemonic(LDR(), Bit8(), _), rt, RegisterMemoryReference(rn, rm, index, add, wback, shift_t, shift_n))
        if rt != PC() && rm != PC() && !(wback && (rn == PC() || rn == rt)) =>

        val offset = shift(rm, shift_t, shift_n, C)
        val offset_addr = if (add) rn.asVar + offset else rn.asVar - offset
        val address = if (index) offset_addr else rn.asVar
        for {
          byte <- readMemU(address, 1)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield (rt.asVar |-> byte.zext(32 - 8)) ++ writeBack

      // LDRD (immediate) -- p. 3180
      case MemoryInstruction(MemoryMnemonic(LDR(), Bit64(), _), rt1, ImmediateMemoryReference(rn, imm, index, add, wback)) if rn != PC() && rt1.registerIndex % 2 == 0 && rt1 != R14() &&
        !(wback && (rn == rt1 || rn.registerIndex == rt1.registerIndex + 1)) =>

        val rt2 = register(rt1.registerIndex + 1)
        val offset_addr = if (add) rn.asVar + imm.bvu else rn.asVar - imm.bvu
        val address = if (index) offset_addr else rn.asVar
        for {
          byte1 <- readMemA(address, 4)
          byte2 <- readMemA(address + 4.bvu, 4)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield (rt1.asVar |-> byte1) ++ (rt2.asVar |-> byte2) ++ writeBack

      // LDRD (literal) -- p. 3183
      case MemoryInstruction(MemoryMnemonic(LDR(), Bit64(), _), rt1, ImmediateMemoryReference(PC(), imm, true, add, false)) if rt1.registerIndex % 2 == 0 =>
        val rt2 = register(rt1.registerIndex + 1)
        val address = if (add) align(pc, 4) + imm.bvu else align(pc, 4) - imm.bvu
        for {
          byte1 <- readMemA(address, 4)
          byte2 <- readMemA(address + 4.bvu, 4)
        } yield (rt1.asVar |-> byte1) ++ (rt2.asVar |-> byte2)

      // LDRD (register) -- p. 3186
      case MemoryInstruction(MemoryMnemonic(LDR(), Bit64(), _), rt1, RegisterMemoryReference(rn, rm, index, add, wback, _, 0))
        if rt1.registerIndex % 2 == 0 && rt1 != R14() && rm != PC() && rm != rt1 && rm.registerIndex != rt1.registerIndex + 1 &&
          !(wback && (rn == PC() || rn == rt1 || rn.registerIndex == rt1.registerIndex + 1)) =>

        val rt2 = register(rt1.registerIndex + 1)
        val offset_addr = if (add) rn.asVar + rm else rn.asVar - rm
        val address = if (index) offset_addr else rn.asVar
        for {
          byte1 <- readMemA(address, 4)
          byte2 <- readMemA(address + 4.bvu, 4)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield (rt1.asVar |-> byte1) ++ (rt2.asVar |-> byte2) ++ writeBack

      // LDRH (immediate) -- p. 3196
      case MemoryInstruction(MemoryMnemonic(LDR(), Bit16(), _), rt, ImmediateMemoryReference(rn, imm, index, add, wback)) if rn != PC() && rt != PC() && !(wback && rn == rt) =>
        val offset_addr = if (add) rn.asVar + imm.bvu else rn.asVar - imm.bvu
        val address = if (index) offset_addr else rn.asVar
        for {
          data <- readMemU(address, 2)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield (rt.asVar |-> data.zext(32 - 16)) ++ writeBack

      // LDRH (register) -- p. 3202
      case MemoryInstruction(MemoryMnemonic(LDR(), Bit16(), _), rt, RegisterMemoryReference(rn, rm, index, add, wback, shift_t, shift_n))
        if rt != PC() && rm != PC() && !(wback && (rn == PC() || rn == rt)) =>

        val offset = shift(rm, shift_t, shift_n, C)
        val offset_addr = if (add) rn.asVar + offset else rn.asVar - offset
        val address = if (index) offset_addr else rn.asVar
        for {
          data <- readMemU(address, 2)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield (rt.asVar |-> data.zext(32 - 16)) ++ writeBack

      // LDRSB (immediate) -- p. 3208
      case MemoryInstruction(MemoryMnemonic(LDR(), SBit8(), _), rt, ImmediateMemoryReference(rn, imm, index, add, wback)) if rn != PC() && rt != PC() && !(wback && rn == rt) =>
        val offset_addr = if (add) rn.asVar + imm.bvu else rn.asVar - imm.bvu
        val address = if (index) offset_addr else rn.asVar
        for {
          data <- readMemU(address, 1)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield (rt.asVar |-> data.sext(32 - 8)) ++ writeBack

      // LDRSH (immediate) -- p. 3219
      case MemoryInstruction(MemoryMnemonic(LDR(), SBit16(), _), rt, ImmediateMemoryReference(rn, imm, index, add, wback)) if rn != PC() && rt != PC() && !(wback && rn == rt) =>
        val offset_addr = if (add) rn.asVar + imm.bvu else rn.asVar - imm.bvu
        val address = if (index) offset_addr else rn.asVar
        for {
          data <- readMemU(address, 2)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield (rt.asVar |-> data.sext(32 - 16)) ++ writeBack

      // STR (immediate) -- p. 3508
      case MemoryInstruction(MemoryMnemonic(STR(), Bit32(), _), rt, ImmediateMemoryReference(rn, imm, index, add, wback)) if !(wback && (rn == PC() || rn == rt)) =>
        val offset_addr = if (add) rn.asVar + imm.bvu else rn.asVar - imm.bvu
        val address = if (index) offset_addr else rn.asVar
        for {
          update <- writeMemU(address, 4, if (rt == PC()) pcStoreValue() else rt)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield update ++ writeBack

      // STR (register) -- p. 3512
      case MemoryInstruction(MemoryMnemonic(STR(), Bit32(), _), rt, RegisterMemoryReference(rn, rm, index, add, wback, shift_t, shift_n)) if rm != PC() && !(wback && (rn == PC() || rn == rt)) =>
        val offset = shift(rm, shift_t, shift_n, C)
        val offset_addr = if (add) rn.asVar + offset else rn.asVar - offset
        val address = if (index) offset_addr else rn.asVar
        for {
          update <- writeMemU(address, 4, if (rt == PC()) pcStoreValue() else rt)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield update ++ writeBack

      // STRB (immediate) -- p. 3515
      case MemoryInstruction(MemoryMnemonic(STR(), Bit8(), _), rt, ImmediateMemoryReference(rn, imm, index, add, wback)) if rt != PC() && !(wback && (rn == PC() || rn == rt)) =>
        val offset_addr = if (add) rn.asVar + imm.bvu else rn.asVar - imm.bvu
        val address = if (index) offset_addr else rn.asVar
        for {
          u <- writeMemU(address, 1, rt.asVar.extract(7, 0))
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield u ++ writeBack

      // STRB (register) -- p. 3519
      case MemoryInstruction(MemoryMnemonic(STR(), Bit8(), _), rt, RegisterMemoryReference(rn, rm, index, add, wback, shift_t, shift_n)) if rt != PC() && rm != PC() && !(wback && (rn == PC() || rn == rt)) =>
        val offset = shift(rm, shift_t, shift_n, C)
        val offset_addr = if (add) rn.asVar + offset else rn.asVar - offset
        val address = if (index) offset_addr else rn.asVar
        for {
          u <- writeMemU(address, 1, rt.asVar.extract(7, 0))
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield u ++ writeBack

      // STRD (immediate) -- p. 3526
      case MemoryInstruction(MemoryMnemonic(STR(), Bit64(), _), rt1, ImmediateMemoryReference(rn, imm, index, add, wback))
        if rt1.registerIndex % 2 == 0 && !(wback && (rn == PC() || rn == rt1 || rn.registerIndex == rt1.registerIndex + 1)) && rt1 != R14() =>

        val rt2 = register(rt1.registerIndex + 1)

        val offset_addr = if (add) rn.asVar + imm.bvu else rn.asVar - imm.bvu
        val address = if (index) offset_addr else rn.asVar
        for {
          u1 <- writeMemA(address, 4, rt1)
          u2 <- writeMemA(address + 4.bvu, 4, rt2)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield u1 ++ u2 ++ writeBack

      // STRD (register) -- p. 3530
      case MemoryInstruction(MemoryMnemonic(STR(), Bit64(), _), rt1, RegisterMemoryReference(rn, rm, index, add, wback, _, 0))
        if rt1.registerIndex % 2 == 0 && rt1 != R14() && rm != PC() && !(wback && (rn == PC() || rn == rt1 || rn.registerIndex == rt1.registerIndex + 1)) =>

        val rt2 = register(rt1.registerIndex + 1)

        val offset_addr = if (add) rn.asVar + rm else rn.asVar - rm
        val address = if (index) offset_addr else rn.asVar
        for {
          u1 <- writeMemA(address, 4, rt1)
          u2 <- writeMemA(address + 4.bvu, 4, rt2)
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield u1 ++ u2 ++ writeBack

      // STRH (immediate) -- p. 3544
      case MemoryInstruction(MemoryMnemonic(STR(), Bit16(), _), rt, ImmediateMemoryReference(rn, imm, index, add, wback)) if rt != PC() && !(wback && (rn == PC() || rn == rt)) =>
        val offset_addr = if (add) rn.asVar + imm.bvu else rn.asVar - imm.bvu
        val address = if (index) offset_addr else rn.asVar
        for {
          u <- writeMemU(address, 2, rt.asVar.extract(15, 0))
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield u ++ writeBack

      // STRH (register) -- p. 3548
      case MemoryInstruction(MemoryMnemonic(STR(), Bit16(), _), rt, RegisterMemoryReference(rn, rm, index, add, wback, shift_t, shift_n))
        if rt != PC() && rm != PC() && !(wback && (rn == PC() || rn == rt)) =>

        val offset = shift(rm, shift_t, shift_n, C)
        val offset_addr = if (add) rn.asVar + offset else rn.asVar - offset
        val address = if (index) offset_addr else rn.asVar

        for {
          u <- writeMemU(address, 2, rt.asVar.extract(15, 0))
          writeBack = if (wback) rn.asVar |-> offset_addr else noUpdate
        } yield u ++ writeBack

      // MOV, MOVS (immediate) -- p. 3257
      case MoveInstruction(MoveMnemonic(MOV(), sFlag, _), rd, ConstantOperand(imm)) if rd != PC() || sFlag == NoSFlag() =>
        if (rd == PC())
          aluWritePC(imm.bvs)
        else
          (rd.asVar |-> imm.bvs) ++ setFlags(sFlag, imm.testBit(31), imm == 0, extractCarry(imm), V)

      // MOV, MOVS (register) -- p. 3260
      case MoveInstruction(MoveMnemonic(MOV(), sFlag, _), rd, SimpleRegisterShift(rm, shift_t, shift_n)) if rd != PC() || sFlag == NoSFlag() =>
        val (result, carry) = shiftC(rm, shift_t, shift_n, C)
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), carry, V)

      // MOV (register-shifted register) -- p. 3265
      case MoveInstruction(MoveMnemonic(MOV(), sFlag, _), rd, RegisterShiftReg(rm, shift_t, rs)) if rd != PC() && rm != PC() && rs != PC() =>
        val shift_n = UInt(rs.asVar.extract(7, 0))(WordSize.Byte)
        val (result, carry) = shiftC(rm, toSRType(shift_t), shift_n, C)
        (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), carry, V)

      // MVN, MVNS (immediate) -- p. 3292
      case MoveInstruction(MoveMnemonic(MVN(), sFlag, _), rd, ConstantOperand(imm)) if rd != PC() || sFlag == NoSFlag() =>
        val result = !imm.bvs
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, !imm.testBit(31), isZero(result), extractCarry(imm), V)

      // MVN, MVNS (register) -- p. 3294
      case MoveInstruction(MoveMnemonic(MVN(), sFlag, _), rd, SimpleRegisterShift(rm, shift_t, shift_n)) if rd != PC() || sFlag == NoSFlag() =>
        val (shifted, carry) = shiftC(rm, shift_t, shift_n, C)
        val result = !shifted
        if (rd == PC())
          aluWritePC(result)
        else
          (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), carry, V)

      // MVN, MVNS (register-shifted register) -- p. 3297
      case MoveInstruction(MoveMnemonic(MVN(), sFlag, _), rd, RegisterShiftReg(rm, shift_t, rs)) if !Set(rd, rm, rs).contains(PC()) =>
        val shift_n = UInt(rs.asVar.extract(7, 0))(WordSize.Byte)
        val (shifted, carry) = shiftC(rm, toSRType(shift_t), shift_n, C)
        val result = !shifted
        (rd.asVar |-> result) ++ setFlags(sFlag, result(31), isZero(result), carry, V)

      // NOP -- p. 3299
      case NopInstruction(NopMnemonic(_)) =>
        noUpdate

      // B -- p. 3051
      case StaticJumpInstruction(B(_), target, _) =>
        branchWritePC(target.bvu)

      // BL (immediate) -- p. 3067
      case StaticJumpInstruction(BL(_), target, _) =>
        branchWritePC(target.bvu) ++ (lr |-> pc - 4.bvu)

      // BX -- p. 3071
      case DynamicJumpInstruction(DynamicJumpMnemonic(BX(), _), rm, _) =>
        bxWritePC(rm)

      // BLX (register) -- p. 3069
      case DynamicJumpInstruction(DynamicJumpMnemonic(DynBLX(), _), rm, _) =>
        for {
          update <- bxWritePC(rm)
        } yield (lr |-> pc - 4.bvu) ++ update

      case _ =>
        logger.error(s"Semantics undefined for instruction $i")
        Semantics.undefined
    }
  }

  /// Helpers & syntactic sugar
  /// -------------------------

  private def toSRType(shift: Shift): SRType = shift match {
    case ASR() => SRType.ASR
    case LSL() => SRType.LSL
    case LSR() => SRType.LSR
    case ROR() => SRType.ROR
  }

  private def setFlags(flag: SFlagTag, n: Formula, z: Formula, c: Formula, v: Formula): StateUpdate = {
    if (flag == SFlag())
      (N |-> n) ++ (Z |-> z) ++ (C |-> c) ++ (V |-> v)
    else
      TypedMap.empty
  }

  // p. 2864
  private def extractCarry(imm: BigInt): Formula = {
    if ((0 to 255).contains(imm))
      C
    else
      imm.testBit(31)
  }

  private lazy val noUpdate: StateUpdate = TypedMap.empty

  private def pc = PC().asVar

  private def lr = LR().asVar

  private def sp = SP().asVar

  /// Implicit conversions
  /// --------------------

  implicit def toSemantics(s: Guarded[StateUpdate]): Semantics = new Semantics {
    override val isDefined: Set[StatePredicate] = s.guards
    override val update: StateUpdate = s.value
  }

  implicit def toSemantics(s: StateUpdate): Semantics = new Semantics {
    override val isDefined: Set[StatePredicate] = Set.empty
    override val update: StateUpdate = s
  }

  // Applies the right operand in the state created by applying the update.
  implicit class StateUpdateOps(update: StateUpdate) {
    def |>[T](expr: Expr[T]): Expr[T] = expr.substitute(update)

    def |>[T](exprs: Set[Expr[T]]): Set[Expr[T]] = exprs.map(this |> _)

    def |>[T](guarded: Guarded[Expr[T]]): Guarded[Expr[T]] = Guarded(this |> guarded.guards, this |> guarded.value)

    def |>>(nextUpdate: StateUpdate): StateUpdate = nextUpdate.mapValues(new TypedFunction[Expr, Expr] {
      override def apply[I](x: Expr[I]): Expr[I] = StateUpdateOps.this |> x
    })

    def |>>(guardedUpdate: Guarded[StateUpdate]): Guarded[StateUpdate] = Guarded(this |> guardedUpdate.guards, this |>> guardedUpdate.value)

    def +>>(nextUpdate: StateUpdate): StateUpdate = update ++ (this |>> nextUpdate)

    def +>>(guardedUpdate: Guarded[StateUpdate]): Guarded[StateUpdate] = Guarded(this |> guardedUpdate.guards, this +>> guardedUpdate.value)
  }

  /// Operand decoding patterns
  /// -------------------------

  object SimpleRegisterShift {
    def unapply(op: FlexibleOperand): Option[(Register, SRType, Int)] = op match {
      case RegisterOperand(reg)                => Some(reg, SRType.LSL, 0)
      case RegisterRRX(reg)                    => Some(reg, SRType.RRX, 1)
      case RegisterShiftConst(reg, shift, imm) => Some(reg, toSRType(shift), imm)
      case _                                   => None
    }
  }

  object ImmediateMemoryReference {
    def unapply(ref: MemoryRef): Option[( /*base: */ Register, /*imm: */ BigInt, /*index: */ Boolean, /*add: */ Boolean, /*wback: */ Boolean)] = ref match {
      case NoOffset(base)                         => Some(base, 0, true, true, false)
      case UnIndexed(base, ConstantOffset(imm))   => Some(base, imm.abs, true, imm >= 0, false)
      case PostIndexed(base, ConstantOffset(imm)) => Some(base, imm.abs, false, imm >= 0, true)
      case PreIndexed(base, ConstantOffset(imm))  => Some(base, imm.abs, true, imm >= 0, true)
      case _                                      => None
    }
  }

  object RegisterMemoryReference {
    // TODO: syntax does not support subtraction! add is always true
    // TODO: should RRX be a supported shift here? Syntax does not support it.
    def unapply(ref: MemoryRef): Option[( /*base: */ Register, /*rm: */ Register, /*index: */ Boolean, /*add: */ Boolean, /*wback: */ Boolean, /*shift_t: */ SRType, /*shift_n: */ Int)] = ref match {
      case UnIndexed(base, RegisterOffset(rm))   => Some(base, rm, true, true, false, SRType.LSL, 0)
      case PostIndexed(base, RegisterOffset(rm)) => Some(base, rm, false, true, true, SRType.LSL, 0)
      case PreIndexed(base, RegisterOffset(rm))  => Some(base, rm, true, true, true, SRType.LSL, 0)

      case UnIndexed(base, ScaledOffset(rm, shift, imm))   => Some(base, rm, true, true, false, toSRType(shift), imm)
      case PostIndexed(base, ScaledOffset(rm, shift, imm)) => Some(base, rm, false, true, true, toSRType(shift), imm)
      case PreIndexed(base, ScaledOffset(rm, shift, imm))  => Some(base, rm, true, true, true, toSRType(shift), imm)

      case _ => None
    }
  }

}
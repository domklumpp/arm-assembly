package au.edu.mq.comp.assembly
package arm

import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax.Register
import au.edu.mq.comp.assembly.semantics._
import au.edu.mq.comp.assembly.smt.Syntax._

import org.bitbucket.franck44.scalasmt.theories.BVTerm

package object semantics {
  type BV = Expr[BVTerm]

  implicit def registerAsVariable(register: Register): ProgramVariable[BVTerm] = BVs(register.getClass.getSimpleName.toLowerCase, WordSize.Bits32)

  implicit class RegisterOp(r: Register) {
    def asVar: ProgramVariable[BVTerm] = registerAsVariable(r)
  }

  /** A class encapsulating the word size to be used as length of bit vectors. */
  sealed case class WordSize(size: Int) {
    def +(other: Int): WordSize = WordSize(size + other)
  }

  object WordSize {
    val Bit = WordSize(1)
    val Byte = WordSize(8)
    val Bits32 = WordSize(32)
    val Bits64 = WordSize(64)

    def max(a: WordSize, b: WordSize): WordSize = WordSize(scala.math.max(a.size, b.size))
  }

  implicit def wordSizeToInt(N: WordSize): Int = N.size

  implicit class BigIntOps(i: BigInt) {
    def i32: BV = bvs(WordSize.Bits32)

    def ui32: BV = bvu(WordSize.Bits32)

    def bvu(implicit N: WordSize): BV = BVs(i, N, unsigned = true)

    def bvs(implicit N: WordSize): BV = BVs(i, N)
  }

  implicit class IntOps(i: Int) {
    def i32: BV = bvs(WordSize.Bits32)

    def ui32: BV = bvu(WordSize.Bits32)

    def bvu(implicit N: WordSize): BV = i.withUBits(N)

    def bvs(implicit N: WordSize): BV = i.withBits(N)
  }

  implicit class BitVectorOps(bv: BV) {
    def apply(index: Int): Formula = bv.extract(index, index) === 1.withUBits(1)
  }

  implicit class FormulaOps(f: Formula) {
    def bit: BV = f.ifThenElse(1.withUBits(1), 0.withUBits(1))
  }

  def combine[I](left: SemanticsDefinition[I], right: SemanticsDefinition[I]): SemanticsDefinition[I] = i => new Semantics {
    override val isDefined: Set[StatePredicate] = left(i).isDefined ++ right(i).isDefined
    override val update: StateUpdate = left(i).update ++ right(i).update
  }
}

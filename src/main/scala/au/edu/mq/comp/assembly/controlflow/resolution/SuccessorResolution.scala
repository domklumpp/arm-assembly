package au.edu.mq.comp.assembly
package controlflow
package resolution

import scala.util.Try

/**
  * Represents different strategies to resolve possible successor locations for traces.
  *
  * @tparam I1 The type of instructions in the underlying program.
  * @tparam I2 The concrete type of instructions in the trace.
  */
trait SuccessorResolution[I1, I2] {
  /**
    * Resolves the possible locations after executing the given trace in the given program.
    *
    * @param program  The program in which this trace is executed.
    * @param alphabet The complete alphabet the computed resolver can use.
    * @param existing The previously computed resolver. Can be used to avoid computing redundant information.
    * @param trace    The (concrete) trace, where each instruction is labeled with its location. This must only use instructions in the given alphabet.
    * @param parallel For each instruction in the trace, a set of instructions in parallel traces at the same index.
    * @return A resolver that at least accepts the given trace, but may accept more traces.
    *         For each accepted trace tau, the label of the final state is a superset of possible final locations
    *         when executing tau.
    */
  def resolve(program: Program[I1], alphabet: Set[(Location, I2)], existing: PredicateAutomatonResolver[(Location, I2)])
             (trace: LabeledTrace[I2], parallel: Seq[Set[(Location, I2)]]): Try[PredicateAutomatonResolver[(Location, I2)]]

  /**
    * Combines two resolution strategies: if the first one fails, the second is invoked.
    *
    * @param other The fallback strategy.
    */
  final def +>(other: SuccessorResolution[I1, I2]): SuccessorResolution[I1, I2] = FallbackSuccessorResolution(this, other)
}

private case class FallbackSuccessorResolution[I1, I2](first: SuccessorResolution[I1, I2], second: SuccessorResolution[I1, I2]) extends SuccessorResolution[I1, I2] {
  override def resolve(program: Program[I1], alphabet: Set[(Location, I2)], existing: PredicateAutomatonResolver[(Location, I2)])
                      (trace: LabeledTrace[I2], parallel: Seq[Set[(Location, I2)]]): Try[PredicateAutomatonResolver[(Location, I2)]] =
    first.resolve(program, alphabet, existing)(trace, parallel)
      .orElse(second.resolve(program, alphabet, existing)(trace, parallel))
}
package au.edu.mq.comp.assembly
package smt

import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.SSymbol
import org.bitbucket.franck44.scalasmt.typedterms.VarTerm

/**
  * A map associating program variable to logic variables of the same type.
  *
  * @param indexMap Mappings from program variable names to current index.
  */
final case class VariableMap(private val indexMap: Map[String, Int]) extends TypedFunction[ProgramVariable, VarTerm] {
  private val defaultIndex = 0

  /**
    * Retrieves the logic variable representing the given program variable.
    *
    * @param v The program variable.
    * @tparam T The program variable's type.
    * @return The stored logic variable, or a fresh logic variable indexed with 0 if no stored logic variable is found.
    */
  override def apply[T](v: ProgramVariable[T]): VarTerm[T] = v.indexed(getIndex(v))

  /**
    * Creates a new variable map by increasing the index of the logic variable associated with the given program variable.
    *
    * @param variable The program variable.
    * @tparam T the program variable's type
    * @return A new variable map (the original map is unchanged).
    */
  def bump[T](variable: ProgramVariable[T]): VariableMap = {
    VariableMap(indexMap + (variable.id -> (getIndex(variable) + 1)))
  }

  /**
    * Retrieves the current index associated with the given logic variable; or 0 if no index is associated.
    */
  private def getIndex(x: ProgramVariable[_]): Int = getIndex(x.id)

  /**
    * Retrieves the current index associated with the logic variable with the given name; or 0 if no index is associated.
    */
  private def getIndex(x: String): Int = indexMap.getOrElse(x, defaultIndex)

  /**
    * Replaces program variables in an expression by the corresponding logic variable.
    *
    * @param expr The quantifier-free expression to manipulate.
    * @tparam T The expression's logical type.
    * @return A new expression where all occurrences of program variables have been replaced with logic variables.
    */
  def substitute[T](expr: Expr[T]): Expr[T] = expr.indexedBy { case SSymbol(programVariable) => getIndex(programVariable) }
}

object VariableMap {
  /**
    * A VariableMap with no existing mappings.
    */
  lazy val initial: VariableMap = VariableMap(Map.empty)
}
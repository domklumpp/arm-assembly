package au.edu.mq.comp.assembly
package arm.semantics

import au.edu.mq.comp.assembly.smt.Syntax._

/**
  * Support for pseudo-integer calculations with bit vectors by transparent application of the necessary extensions.
  */
object IntegerSupport {
  implicit def intAsInteger(i: Int): Integer = Integer(i)

  implicit def bigIntAsInteger(i: BigInt): Integer = Integer(i)

  object UInt {
    /**
      * Interprets a bit vector as unsigned integer.
      *
      * @param bv The bit vector to translate to an integer.
      * @param N  The bit vector's size.
      */
    def apply(bv: BV)(implicit N: WordSize): Integer = UBVInteger(bv, N)

    /**
      * A bit vector expressing the greatest unsigned integer possible with the given number of bits.
      */
    def max(implicit N: WordSize): BV = BVs(BigInt(2).pow(N) - 1, N, unsigned = true)
  }

  object SInt {
    /**
      * Interprets a bit vector as signed integer.
      *
      * @param bv The bit vector to translate to an integer.
      * @param N  The bit vector's size.
      */
    def apply(bv: BV)(implicit N: WordSize): Integer = SBVInteger(bv, N)

    /**
      * A bit vector expressing the greatest signed integer possible with the given number of bits.
      */
    def max(implicit N: WordSize): BV = BVs(BigInt(2).pow(N - 1) - 1, N)

    /**
      * A bit vector expressing the smallest signed integer possible with the given number of bits.
      */
    def min(implicit N: WordSize): BV = BVs(-BigInt(2).pow(N - 1), N)
  }

  object Integer {
    /**
      * Converts an Int to an Integer.
      */
    def apply(i: Int): Integer = apply(BigInt(i))

    /**
      * Converts a BigInt to an Integer.
      */
    def apply(i: BigInt): Integer = BigIntInteger(i)
  }

  trait Integer {
    /**
      * The minimum size in bits required to express this integer in 2's complement representation.
      */
    def requiredSize: WordSize

    /**
      * Represents this instance as bit vector in 2's complement representation.
      *
      * @param size The number of bits to use (must be greater or equal the required size).
      */
    def asBitVector(size: WordSize): BV

    /**
      * Represents this instance as bit vector in 2's complement representation, using minimum requireed number of bits.
      */
    def bv: BV = asBitVector(requiredSize)

    def +(other: Integer): Integer = Operation(this, _ + _, other, WordSize.max(requiredSize, other.requiredSize) + 1)

    def -(other: Integer): Integer = Operation(this, _ - _, other, WordSize.max(requiredSize, other.requiredSize) + 1)

    def *(other: Integer): Integer = Operation(this, _ * _, other, requiredSize + other.requiredSize)

    def /(other: Integer): Integer = Operation(this, _ sdiv _, other, WordSize.max(requiredSize, other.requiredSize))

    def %(other: Integer): Integer = Operation(this, _ % _, other, WordSize.max(requiredSize, other.requiredSize))

    def unary_- : Integer = Negative(this)

    def ===(other: Integer): Formula = {
      val commonSize = WordSize.max(requiredSize, other.requiredSize)
      asBitVector(commonSize) === other.asBitVector(commonSize)
    }

    def =/=(other: Integer): Formula = {
      val commonSize = WordSize.max(requiredSize, other.requiredSize)
      asBitVector(commonSize) =/= other.asBitVector(commonSize)
    }

    def extract(high: Int, low: Int): BV = asBitVector(WordSize(scala.math.max(high + 1, requiredSize))).extract(high, low)

    def literalValue: Option[BigInt] = None
  }

  private case class UBVInteger(bitVector: BV, actualSize: WordSize) extends Integer {
    override def requiredSize: WordSize = actualSize + 1

    override def asBitVector(size: WordSize): BV = {
      require(size >= requiredSize, s"Size $size is less than required size $requiredSize.")
      if (size > requiredSize)
        bitVector.zext(size - actualSize)
      else
        bitVector
    }
  }

  private case class SBVInteger(bitVector: BV, requiredSize: WordSize) extends Integer {
    override def asBitVector(size: WordSize): BV = {
      require(size >= requiredSize, s"Size $size is less than required size $requiredSize.")
      if (size > requiredSize)
        bitVector.sext(size - requiredSize)
      else
        bitVector
    }
  }

  private case class BigIntInteger(i: BigInt) extends Integer {
    override def requiredSize: WordSize = WordSize(i.bitLength + 1)

    override def asBitVector(size: WordSize): BV = {
      require(size >= requiredSize, s"Size $size is less than required size $requiredSize.")
      i.bvs(size)
    }

    override def +(other: Integer): Integer = other match {
      case BigIntInteger(j) => BigIntInteger(i + j)
      case _                => super.+(other)
    }

    override def -(other: Integer): Integer = other match {
      case BigIntInteger(j) => BigIntInteger(i - j)
      case _                => super.-(other)
    }

    override def *(other: Integer): Integer = other match {
      case BigIntInteger(j) => BigIntInteger(i * j)
      case _                => super.*(other)
    }

    override def /(other: Integer): Integer = other match {
      case BigIntInteger(j) => BigIntInteger(i / j)
      case _                => super./(other)
    }

    override def %(other: Integer): Integer = other match {
      case BigIntInteger(j) => BigIntInteger(i % j)
      case _                => super.%(other)
    }

    override def unary_- : Integer = BigIntInteger(-i)

    override def ===(other: Integer): Formula = other match {
      case BigIntInteger(j) => i == j
      case _                => super.===(other)
    }

    override def =/=(other: Integer): Formula = other match {
      case BigIntInteger(j) => i != j
      case _                => super.=/=(other)
    }

    override def literalValue: Option[BigInt] = Some(i)
  }

  private case class Operation(a: Integer, op: (BV, BV) => BV, b: Integer, requiredSize: WordSize) extends Integer {
    override def asBitVector(size: WordSize): BV = {
      require(size >= requiredSize, s"Size $size is less than required size $requiredSize.")
      op(a.asBitVector(size), b.asBitVector(size))
    }
  }

  private case class Negative(a: Integer) extends Integer {
    override def requiredSize: WordSize = a.requiredSize + 1

    override def asBitVector(size: WordSize): BV = {
      require(size >= requiredSize, s"Size $size is less than required size $requiredSize.")
      -a.asBitVector(size)
    }
  }

}

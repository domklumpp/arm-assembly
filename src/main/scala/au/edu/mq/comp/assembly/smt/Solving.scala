package au.edu.mq.comp.assembly
package smt

import org.bitbucket.franck44.scalasmt.configurations.SMTLogics.SMTLogics
import org.bitbucket.franck44.scalasmt.interpreters.{Resources, SMTSolver}
import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._
import org.bitbucket.franck44.scalasmt.typedterms.{Commands, TypedTerm}

import scala.util.Try

/**
  * Provides access to SMT solver capabilities.
  */
object Solving extends Commands with Resources {
  /**
    * Calls scalaSMT's getValue, but wraps the result in a TypedTerm.
    */
  def getTypedValue[T](term: Expr[T])(implicit solver: SMTSolver): Try[Expr[T]] = {
    for (value <- getValue(term))
      yield new TypedTerm[T, Term](Set.empty, value.t)
  }

  /**
    * Checks if the given set of formulas is satisfiable.
    *
    * @param constraints The set of formulas.
    * @param smtLogic    The logic in which the formulas are expressed.
    * @param smtCtx      The SMT solving context to use.
    */
  def isSat(constraints: Set[Formula], smtLogic: SMTLogics)(implicit smtCtx: SolverContext): Try[Boolean] = {
    val result = smtCtx.withSolver(smtLogic) { implicit solver =>
      for {
        _ <- constraints.tryMapAll(|=)
        s <- checkSat()
      } yield s
    }

    // Do this check outside the solver context, as it does not represent a failure of the solver script.
    // Such failures in the context may lead the context to consider a solver damaged and destroy it.
    for (s <- result if s != UnKnown())
      yield s == Sat()
  }
}

package au.edu.mq.comp.assembly
package smt

import org.bitbucket.franck44.scalasmt.configurations.SMTLogics.SMTLogics

import scala.util.{Success, Try}

/**
  * Represents an encoding of a trace in SMT formulas.
  *
  * @param initial    Formulas constraining initial states.
  * @param conditions The definedness conditions for each instruction in the trace.
  * @param updates    The update formulas for each instruction in the trace.
  */
final case class TraceEncoding(initial: Set[Formula], conditions: Seq[Set[Formula]], updates: Seq[Set[Formula]], states: Seq[VariableMap]) {

  import au.edu.mq.comp.assembly.smt.Syntax.FormulaSetExtensions

  /**
    * The set of all formulas in the encoding.
    */
  lazy val formulas: Set[Formula] = initial ++ conditions.flatten ++ updates.flatten

  /**
    * A sequence of formulas, each encoding the effect of one instruction of the trace.
    */
  lazy val traceFormulas: Seq[Formula] = conditions.zip(updates).map { case (c, u) => (c ++ u).conjunction }
}

/**
  * Encodes a trace of a program as SMT formulas.
  *
  * @tparam I1 The type of instructions in the program.
  * @tparam I2 The type of (concrete) instructions in the trace.
  */
trait TraceEncoder[I1, I2] {
  /**
    * Encodes the given trace (in the context of the given program) as SMT formulas (in the given SMT logic).
    *
    * @return The trace encoding, and a variable mapping for each state in the trace from program to logic variables.
    */
  def encode(program: Program[I1], trace: Seq[I2], logic: SMTLogics): Try[TraceEncoding]
}

/**
  * A simple default trace encoder using [[SmtEncoder]].
  */
final case class DefaultTraceEncoder[I1, I2: InstructionSet]() extends TraceEncoder[I1, I2] {
  override def encode(program: Program[I1], trace: Seq[I2], logic: SMTLogics): Try[TraceEncoding] = {
    val (traceFormulas, states) = SmtEncoder.encodeTraceFormulas(trace)
    val (conditions, updates) = traceFormulas.unzip
    val initial = program.initialStatePredicates.map(states.head.substitute)
    Success(TraceEncoding(initial, conditions, updates, states))
  }
}

/**
  * A compositional trace encoder that projects the set of formulas to those relevant to the value of a given target expression.
  *
  * @param underlying The underlying trace encoder to be used.
  * @param targetExpr An expression whose value is of interest after execution of the trace.
  * @tparam I1 The type of instructions in the program.
  * @tparam I2 The type of (concrete) instructions in the trace.
  */
final case class SimpleProjectingTraceEncoder[I1, I2](underlying: TraceEncoder[I1, I2], targetExpr: Expr[_]) extends TraceEncoder[I1, I2] {

  import au.edu.mq.comp.assembly.memoize

  import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.QualifiedId

  override def encode(program: Program[I1], trace: Seq[I2], logic: SMTLogics): Try[TraceEncoding] = {
    for {
      encoding <- underlying.encode(program, trace, logic)
      (_, projection) = closureProjection(encoding.formulas, FreeVariables.compute(encoding.states.last.substitute(targetExpr)))
      projectedConditions = encoding.conditions.map(_ & projection)
      projectedUpdates = encoding.updates.map(_ & projection)
    } yield TraceEncoding(encoding.initial & projection, projectedConditions, projectedUpdates, encoding.states)
  }

  private def closureProjection(formulas: Set[Formula], variables: Set[QualifiedId]): (Set[QualifiedId], Set[Formula]) =
    memoize {
      closureProjection(_: Set[QualifiedId], Set.empty, _: Set[Formula])
    }(variables, formulas)

  private def closureProjection(closure: Set[QualifiedId], projection: Set[Formula], remaining: Set[Formula]): (Set[QualifiedId], Set[Formula]) = {
    val (newFormulas, newRemaining) = remaining.partition(f => (freeVariables(f) & closure).nonEmpty)
    if (newFormulas.isEmpty)
      (closure, projection)
    else
      closureProjection(closure ++ newFormulas.flatMap(freeVariables), projection ++ newFormulas, newRemaining)
  }

  private def freeVariables(formula: Formula): Set[QualifiedId] = FreeVariables.compute(formula.termDef)
}

/**
  * A compositional trace encoder that, for infeasible traces, eliminates all definedness conditions from the encoding.
  *
  * @param underlying The underlying trace encoder to be used.
  * @param smtCtx     An SMT context to be used for the feasibility check.
  * @tparam I1 The type of instructions in the program.
  * @tparam I2 The type of (concrete) instructions in the trace.
  */
final case class SSAOnlyTraceEncoder[I1, I2](underlying: TraceEncoder[I1, I2])(implicit smtCtx: SolverContext) extends TraceEncoder[I1, I2] {

  import au.edu.mq.comp.assembly.smt.Solving.isSat

  override def encode(program: Program[I1], trace: Seq[I2], logic: SMTLogics): Try[TraceEncoding] = {
    for {
      encoding <- underlying.encode(program, trace, logic)
      sat <- isSat(encoding.formulas, logic)
      projected = if (sat) encoding else encoding.copy(conditions = Seq.fill(trace.length)(Set.empty))
    } yield projected
  }
}

/**
  * A compositional trace encoder that eliminates definedness conditions until the encoding is satisfiable, using UNSAT cores produced by an SMT solver.
  *
  * @param underlying The underlying trace encoder to be used.
  * @param smtCtx     An SMT context to be used for UNSAT core computation.
  * @tparam I1 The type of instructions in the program.
  * @tparam I2 The type of (concrete) instructions in the trace.
  */
final case class UnsatCoreSSATraceEncoder[I1, I2](underlying: TraceEncoder[I1, I2])(implicit smtCtx: SolverContext) extends TraceEncoder[I1, I2] with Logging {

  import au.edu.mq.comp.assembly.smt.Solving._

  import org.bitbucket.franck44.scalasmt.configurations.SMTOptions.UNSATCORES
  import org.bitbucket.franck44.scalasmt.interpreters.SMTSolver
  import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._
  import org.bitbucket.franck44.scalasmt.theories.BoolTerm
  import org.bitbucket.franck44.scalasmt.typedterms.{Named, TypedTerm}

  private type NamedFormula = Named[BoolTerm, NamedTerm]

  require(smtCtx.supportsOption(UNSATCORES), "Solver does not support generation of UNSAT-cores.")

  override def encode(program: Program[I1], trace: Seq[I2], logic: SMTLogics): Try[TraceEncoding] = {
    smtCtx.withSolver(logic, UNSATCORES) { implicit solver =>
      for {
        encoding <- underlying.encode(program, trace, logic)

        nameMap = encoding.conditions.flatten
          .zipWithIndex
          .map { case (cond, i) => (s"condition$i", cond.named(s"condition$i")) }
          .toMap
        namedConditions = nameMap.values.toSet[NamedFormula]

        remainingNamed <- tryFixedPoint(namedConditions)(eliminateConditions(encoding.initial ++ encoding.updates.flatten, nameMap))
        remainingConditions = remainingNamed.map(unnamed)

      } yield encoding.copy(conditions = encoding.conditions.map(_ & remainingConditions))
    }
  }

  private def unnamed(named: NamedFormula): Formula = new TypedTerm[BoolTerm, Term](named.typeDefs, named.aTerm.term)

  private def eliminateConditions(frame: Set[Formula], nameMap: Map[String, NamedFormula])(conditions: Set[NamedFormula])(implicit solver: SMTSolver): Try[Set[NamedFormula]] = {
    for {
      core <- getUnSatCore(frame ++ conditions)
    } yield core match {
      case Some(c@(name :: _)) =>
        logger.debug(s"Found UNSAT core of size ${c.size}. Eliminating formula $name.")
        conditions - nameMap(name) // pick a condition and eliminate it
      case None                =>
        logger.debug("No UNSAT core found, problem is SAT. No more elimination.")
        conditions // nothing left to eliminate
    }
  }

  private def getUnSatCore(formulas: Set[Formula])(implicit solver: SMTSolver): Try[Option[List[String]]] = {
    for {
      _ <- push()
      _ <- formulas.tryMapAll(|=)
      res <- checkSat() if res != UnKnown()

      core <- res match {
        case Sat()   => Success(None)
        case UnSat() => getUnsatCore().map(Some(_))
      }
      _ <- pop()
    } yield core
  }
}

/**
  * A compositional trace encoder that eliminates definedness conditions, but endeavours not to produce an underconstrained problem.
  * It heuristically adds the last conditions if the PC after execution of the trace can be outside the program.
  *
  * @param underlying The underlying trace encoder to be used.
  * @param smtCtx     An SMT context used to check if the PC can have an illegal value.
  * @tparam I1 The type of instructions in the program.
  * @tparam I2 The type of (concrete) instructions in the trace.
  */
final case class ConstrainedSSATraceEncoder[I1, I2: InstructionSet](underlying: TraceEncoder[I1, I2])(implicit smtCtx: SolverContext) extends TraceEncoder[I1, I2] with Logging {

  import au.edu.mq.comp.assembly.smt.Solving._
  import au.edu.mq.comp.assembly.smt.Syntax._

  import org.bitbucket.franck44.scalasmt.interpreters.SMTSolver
  import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._

  private val machine = implicitly[InstructionSet[I2]].machine

  import machine._

  override def encode(program: Program[I1], trace: Seq[I2], logic: SMTLogics): Try[TraceEncoding] = {
    smtCtx.withSolver(logic) { implicit solver =>
      for {
        encoding <- underlying.encode(program, trace, logic)

        // Initial state constraints and SSA should always be satisfiable.
        frame = encoding.initial ++ encoding.updates.flatten
        _ = logFormulas("frame:", frame.toSeq)
        _ <- frame.tryMapAll(|=)

        // PC-checks should not introduce unsatisfiability either, and are especially relevant in case of procedure returns.
        locationChecks = for {
          (conditions, state) <- encoding.conditions.zip(encoding.states.init)
          condition <- conditions if isLocationCheck(state, condition)
        } yield condition
        _ <- locationChecks.tryMapAll(|=)

        // Asserting that the PC has an illegal value should introduce unsatisfiability.
        sanityCheck <- legalLocation(encoding.states.last, program)
        _ = logFormulas("sanity check:", Seq(sanityCheck))
        _ <- |=(sanityCheck)

        // While the problem is still satisfiable, add definedness conditions to constrain it further.
        excludedConditions = encoding.conditions.reverse.toList.flatMap(_ -- locationChecks)
        (included, _) <- tryFixedPoint((locationChecks.toSet, excludedConditions))((selectIncludedCondition _).tupled)

      } yield encoding.copy(conditions = encoding.conditions.map(_ & included))
    }
  }

  private def legalLocation(state: VariableMap, program: Program[I1]): Try[Formula] = {
    for {
      locations <- program.instructionAt.keySet.tryMapAll(locationToExpression)
    } yield locations.map(state.substitute(pcExpr) =/= _).conjunction
  }

  private def isLocationCheck(state: VariableMap, formula: Formula): Boolean = {
    val pcInstance = state.substitute(pcExpr)
    formula.aTerm match {
      case EqualTerm(pcInstance.aTerm, ConstantTerm(_)) => true
      case _                                            => false
    }
  }

  private def selectIncludedCondition(included: Set[Formula], excluded: List[Formula])(implicit solver: SMTSolver): Try[(Set[Formula], List[Formula])] = {
    for {
      sat <- checkSat() if sat != UnKnown()
      result <- includeCondition(sat, included, excluded)
    } yield result
  }

  private def includeCondition(sat: SatResponses, included: Set[Formula], excluded: List[Formula])(implicit solver: SMTSolver): Try[(Set[Formula], List[Formula])] = {
    (excluded, sat) match {
      case (formula :: tail, Sat()) =>
        logger.debug(s"infeasible PC detected -- including ${formula.show}")
        for (_ <- |=(formula)) yield (included + formula, tail)

      case _ => Success((included, excluded))
    }
  }
}
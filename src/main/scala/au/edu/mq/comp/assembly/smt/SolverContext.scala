package au.edu.mq.comp.assembly.smt

import au.edu.mq.comp.assembly.Logging
import au.edu.mq.comp.assembly.smt.Solving._

import org.bitbucket.franck44.scalasmt.configurations.SMTLogics.SMTLogics
import org.bitbucket.franck44.scalasmt.configurations.SMTOptions.SMTOptions
import org.bitbucket.franck44.scalasmt.configurations.{SMTInit, SolverConfig}
import org.bitbucket.franck44.scalasmt.interpreters.SMTSolver

import scala.util.{Failure, Try}

/**
  * A context in which code using an SMT solver can be run.
  */
trait SolverContext {
  /**
    * Runs the given function with an SMT solver.
    * The solver has the given options and logic set, but the assertion stack is empty.
    */
  def withSolver[A](logic: SMTLogics, options: SMTOptions*)(script: SMTSolver => Try[A]): Try[A]

  def supportsOption(option: SMTOptions): Boolean

  def supportsLogic(logic: SMTLogics): Boolean
}

/**
  * A simple context that creates fresh solvers for each call.
  *
  * @param config The SMT solver configuration to use.
  */
final case class FreshSolver(config: SolverConfig) extends SolverContext with Logging {
  override def withSolver[A](logic: SMTLogics, options: SMTOptions*)(script: SMTSolver => Try[A]): Try[A] = {
    logger.debug(s"Creating new solver for $logic using [${options.mkString(", ")}].")
    using(new SMTSolver(config, SMTInit(Some(logic), options)))(script)
  }

  override def supportsOption(option: SMTOptions): Boolean = config.supportedOptions.contains(option)

  override def supportsLogic(logic: SMTLogics): Boolean = config.supportedLogics.contains(logic)
}

/**
  * An SMT context that caches solvers with given options and logic.
  * Successfully terminating scripts are expected to have an equal number of push() and pop() calls.
  * Failing scripts will cause the solver to be removed from the cache.
  */
final case class SolverCache(config: SolverConfig) extends SolverContext with Logging {

  // --------------
  //  Queries
  // --------------

  override def supportsOption(option: SMTOptions): Boolean = config.supportedOptions.contains(option)

  override def supportsLogic(logic: SMTLogics): Boolean = config.supportedLogics.contains(logic)

  // --------------
  //  Execution
  // --------------

  override def withSolver[A](logic: SMTLogics, options: SMTOptions*)(script: SMTSolver => Try[A]): Try[A] = {
    getSolver(options.toList, logic).flatMap { implicit solver =>
      logger.debug(s"Successfully retrieved cached solver for $logic using [${options.mkString(", ")}].")
      val result = run(script)

      result match {
        case Failure(e) =>
          logger.warn(s"Error occurred in solver script: $e")
          logger.warn("Destroying solver.")
          cache.remove((options.toList, logic))
          solver.destroy()

        case _ =>
          logger.debug("Successfully executed script in cached solver.")
      }

      result
    }
  }

  private def run[A](script: SMTSolver => Try[A])(implicit solver: SMTSolver): Try[A] = for {
    _ <- push()
    a <- script(solver)
    _ <- pop()
  } yield a


  // --------------
  //  Solver cache
  // --------------

  import scala.collection.mutable

  private val cache: mutable.HashMap[(List[SMTOptions], SMTLogics), SMTSolver] = new mutable.HashMap()

  private def getSolver(options: List[SMTOptions], logic: SMTLogics): Try[SMTSolver] =
    Try(cache.getOrElseUpdate((options, logic), createSolver(options, logic)))

  private def createSolver(options: List[SMTOptions], logic: SMTLogics): SMTSolver = {
    val solver = new SMTSolver(config, SMTInit(Some(logic), options))
    logger.info(s"Creating new solver for $logic using [${options.mkString(", ")}].")
    solver
  }

  // --------------
  //  Closing
  // --------------

  import au.edu.mq.comp.assembly.CollectionExtensions

  def close(): Unit = cache.values.tryMapAll { s => Try(s.destroy()) }.get
}

object SolverCache {

  import resource.{Resource, managed}

  object SolverCacheResource extends Resource[SolverCache] {
    override def close(r: SolverCache): Unit = r.close()
  }

  def using[A](config: SolverConfig)(f: SolverCache => Try[A]): Try[A] =
    managed(SolverCache(config))
      .map(f)
      .tried
      .flatten
}

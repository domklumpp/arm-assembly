package au.edu.mq.comp.assembly
package arm

import au.edu.mq.comp.assembly.arm.semantics.{BigIntOps, ConditionalSemantics, MemoryRegionSemantics, NopSemantics, UnconditionalInstruction, combine}
import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax.Instruction
import au.edu.mq.comp.assembly.arm.syntax.{ARM, ARMSyntax, Printer}
import au.edu.mq.comp.assembly.semantics.Semantics
import au.edu.mq.comp.assembly.smt.Syntax._

import org.bitbucket.inkytonik.kiama.util.{Positions, StringSource}

import scala.util.Try

/**
  * 32bit ARM instructions for ARM architecture v8.
  */
object AArch32 extends InstructionSet[Instruction] {
  override val machine: Machine = ArmProcessor

  override def parse(source: String): Try[Instruction] = {
    val parser = new ARM(StringSource(source), new Positions())
    val result = parser.pInstruction(0)
    Try(parser.value(result).asInstanceOf[Instruction])
  }

  override def print(instruction: Instruction): String = Printer.show(instruction)

  private val semanticsDefinition = ConditionalSemantics(combine(NopSemantics, MemoryRegionSemantics), NopSemantics.semantics)

  override def semantics(instruction: Instruction): Semantics = semanticsDefinition(instruction)

  /**
    * Returns the initial state predicate for an AArch32 program with the given parameters.
    */
  def initialStatePredicates(initialLocation: Location, instructions: Set[(Location, Instruction)]): Try[Set[StatePredicate]] = {
    val memoryConstraints = instructions.collect { case (location, ARMSyntax.InstrWord(x)) => (location, x.ui32) }
      .flatMap { case (location, data) =>
        (0 to 3).flatMap { i =>
          Set(
            ArmProcessor.memory.select((location + i).ui32) === data.extract(i * 8 + 7, i * 8),
            ArmProcessor.programMemory.select((location + i).ui32) === data.extract(i * 8 + 7, i * 8)
          ): Set[StatePredicate]
        }
      }

    for {
      expr <- ArmProcessor.locationToExpression(initialLocation)
    } yield memoryConstraints + (ArmProcessor.pcExpr === expr)
  }

  /**
    * 32bit ARM instructions for ARM architecture v8, but without conditional execution.
    */
  object Unconditional extends InstructionSet[UnconditionalInstruction] {
    override val machine: Machine = AArch32.machine

    override def parse(source: String): Try[UnconditionalInstruction] = AArch32.parse(source).map(UnconditionalInstruction(_))

    override def print(instruction: UnconditionalInstruction): String = AArch32.print(instruction.instruction)

    private val semanticsDefinition = combine[Instruction](NopSemantics, MemoryRegionSemantics)

    override def semantics(instruction: UnconditionalInstruction): Semantics = semanticsDefinition(instruction.instruction)
  }

  import java.io.File

  def buildProgram(file: File): Try[Program[Instruction]] = {
    import au.edu.mq.comp.assembly.arm.syntax.Normalize

    for {
      parsedProgram <- parse(file)
      normalizedProgram = Normalize.apply(parsedProgram)

      (instructions, words) = normalizedProgram.sections
        .flatMap(_.functions)
        .flatMap(_.optFullInstructions)
        .map { fi => (BigInt(fi.address.hex, 16), fi.instruction) }
        .partition {
          case (_, ARMSyntax.InstrWord(_)) => false
          case _                           => true
        }

      initialLocation <- getEntryPoint(normalizedProgram)
      initial <- AArch32.initialStatePredicates(initialLocation, words.toSet)

    } yield Program(initialLocation, instructions.toMap, initial)
  }

  def parse(file: File): Try[ARMSyntax.Program] = {
    import org.bitbucket.inkytonik.kiama.util.FileSource

    val parser = new ARM(FileSource(file.getAbsolutePath), new Positions())
    val result = parser.pProgram(0)
    Try(parser.value(result).asInstanceOf[ARMSyntax.Program])
  }

  def getEntryPoint(prog: ARMSyntax.Program): Try[Location] = {
    import au.edu.mq.comp.assembly.OptionExtensions

    val functions = prog.sections.flatMap(_.functions)
    for {
      entryFunction <- functions.find(_.name.noBracket == "_Reset")
        .orElse(functions.find(_.name.noBracket == "main"))
        .toTry("Could not find entry point in ARM assembly program. Must be a method named '_Reset' or 'main'.")
    } yield BigInt(entryFunction.optFullInstructions.head.address.hex, 16)
  }
}

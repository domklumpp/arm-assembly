package au.edu.mq.comp.assembly
package arm.controlflow

import au.edu.mq.comp.assembly.arm.ArmProcessor._
import au.edu.mq.comp.assembly.arm.semantics.{BigIntOps, registerAsVariable}
import au.edu.mq.comp.assembly.arm.syntax.PC
import au.edu.mq.comp.assembly.controlflow.UniqueSuccessorAnalysis
import au.edu.mq.comp.assembly.smt.Solving._
import au.edu.mq.comp.assembly.smt.{FreeVariables, NoSimplifier, Simplifier, SolverContext}

import org.bitbucket.franck44.scalasmt.configurations.SMTOptions.MODELS
import org.bitbucket.franck44.scalasmt.theories.BVTerm

import scala.util.{Success, Try}

/**
  * Analyses ARM instructions or instructions of derived instructions sets for their successor locations.
  * The underlying assumption is that the location is uniquely determined by the pc program variable as in the ARM AArch32 semantics.
  *
  * @param simplifier An optional simplification engine that may increase precision of the result.
  */
case class SuccessorAnalysis[I: InstructionSet](simplifier: Simplifier = NoSimplifier)(implicit smtCtx: SolverContext)
  extends UniqueSuccessorAnalysis[I]
    with Logging {

  require(smtCtx.supportsOption(MODELS), "Solver does not support model generation.")
  require(smtCtx.supportsLogic(smtLogic), s"Solver does not support logic $smtLogic.")

  /**
    * A sound but incomplete analysis to detect if an instruction's successor location is uniquely determined by the instruction's location.
    *
    * @param instruction The instruction to analyse.
    * @return Yes, if the instruction's successor location is known to be uniquely determined by the instruction's location. Unknown, if no guarantees can be given.
    */
  override def hasUniqueSuccessor(instruction: I): Option[Boolean] = memoHasUniqueSuccessor(instruction)

  private val memoHasUniqueSuccessor: I => Option[Boolean] = memoize { instruction =>
    logger.debug(s"Checking if ${instruction.print} has unique successor.")

    val instructionSemantics = instruction.semantics
    val update = instructionSemantics.update

    if (!update.isDefined(PC())) {
      logger.debug("Instruction does not update PC.")
      Some(true)
    }
    else {
      val updatedPC = simplifier.simplify(update(PC()), smtLogic, instructionSemantics.isDefined)
      val freeVariables = FreeVariables.compute(updatedPC.aTerm)
      if ((freeVariables - PC().termDef.qualifiedId).isEmpty) {
        logger.debug("Instruction determined to have at most one successor.")
        Some(true)
      }
      else {
        logger.debug("Instruction may have multiple successors.")
        None
      }
    }
  }

  /**
    * Computes an instruction's unique successor location, if possible.
    *
    * @param instruction The instruction. This instruction must have a unique successor as determined by the method above, otherwise None is returned.
    * @param location    The instruction's location.
    * @return A result indicating the unique successor location, if found. Otherwise an indication why none could be found.
    */
  override def computeUniqueSuccessor(location: Location, instruction: I): UniqueSuccessorAnalysis.Result =
    tryComputeSuccessor(location, instruction).getOrElse(UniqueSuccessorAnalysis.Unknown)

  private val tryComputeSuccessor: (Location, I) => Try[UniqueSuccessorAnalysis.Result] = memoizeTry { (location, instruction) =>
    logger.debug(s"Analysing ${instruction.print} for unique successor.")

    val instructionSemantics = instruction.semantics
    val update = instructionSemantics.update

    if (!update.isDefined(PC())) {
      logger.debug("Instruction does not update PC.")
      Success(UniqueSuccessorAnalysis.SingleSuccessor(location))
    }
    else {
      val updatedPC = update(PC()).substitute(PC(), (location + pcOffset).ui32)
      val simplifiedPC = simplifier.simplify(updatedPC, smtLogic, instructionSemantics.isDefined)

      val freeVariables = FreeVariables.compute(simplifiedPC.aTerm)
      if (freeVariables.nonEmpty) {
        logger.debug("Instruction may have multiple successors.")
        Success(UniqueSuccessorAnalysis.Unknown)
      }
      else
        for {
          successor <- evaluateConstant(simplifiedPC)
          realSuccessor = successor - pcOffset
          _ = logger.debug(s"Instruction has at most one successor, $realSuccessor (0x${realSuccessor.toString(16)}).")
        } yield UniqueSuccessorAnalysis.SingleSuccessor(realSuccessor)
    }
  }

  private def evaluateConstant(constant: Expr[BVTerm]): Try[Location] = {
    val result = smtCtx.withSolver(smtLogic, MODELS) { implicit solver =>
      for {
        _ <- checkSat()
        expr <- getTypedValue(constant)
        loc <- expressionToLocation(expr)
      } yield loc
    }

    result.failed.foreach {
      logger.error("Failed to evaluate constant.", _)
    }

    result
  }
}
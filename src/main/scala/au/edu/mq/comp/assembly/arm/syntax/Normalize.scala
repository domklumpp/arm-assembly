package au.edu.mq.comp.assembly.arm.syntax

import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax._

import org.bitbucket.inkytonik.kiama.rewriting.Rewriter.{reduce, rewrite, rule}

object Normalize {
  def apply[T <: ASTNode](x: T): T = rewrite(reduce(normalize))(x)

  private lazy val normalize = eliminateAliases // TODO: other normalizations
  private lazy val eliminateAliases = eliminateStackOperations + eliminateShifts + eliminateRotates // TODO: other aliases

  private lazy val eliminateStackOperations = rule[Instruction] {
    case StackInstruction(StackMnemonic(POP(), cond), List(rt)) =>
      MemoryInstruction(MemoryMnemonic(LDR(), Bit32(), cond), rt, PostIndexed(SP(), ConstantOffset(4)))

    case StackInstruction(StackMnemonic(POP(), cond), registers) if registers.size >= 2 =>
      BatchMemoryInstruction(BatchMemoryMnemonic(LDM(), None, cond), SP(), Update(), registers)

    case StackInstruction(StackMnemonic(PUSH(), cond), List(rt)) =>
      MemoryInstruction(MemoryMnemonic(STR(), Bit32(), cond), rt, PreIndexed(SP(), ConstantOffset(-4)))

    case StackInstruction(StackMnemonic(PUSH(), cond), registers) if registers.size >= 2 =>
      BatchMemoryInstruction(BatchMemoryMnemonic(STM(), Some(DecrementBefore()), cond), SP(), Update(), registers)
  }

  private lazy val eliminateShifts = rule[Instruction] {
    case DataInstruction(DataMnemonic(ShiftOp(shift), sFlag, cond), rd, rm, ConstantOperand(imm)) if imm.isValidInt =>
      MoveInstruction(MoveMnemonic(MOV(), sFlag, cond), rd, RegisterShiftConst(rm, shift, imm.toInt))
    case DataInstruction(DataMnemonic(ShiftOp(shift), sFlag, cond), rd, rm, RegisterOperand(rs))                    =>
      MoveInstruction(MoveMnemonic(MOV(), sFlag, cond), rd, RegisterShiftReg(rm, shift, rs))
  }

  private lazy val eliminateRotates = rule[Instruction] {
    case RotateInstruction(RotateMnemonic(RRX(), sFlag, cond), rd, rm) =>
      MoveInstruction(MoveMnemonic(MOV(), sFlag, cond), rd, RegisterRRX(rm))
  }
}

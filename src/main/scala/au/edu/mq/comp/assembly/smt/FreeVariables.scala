package au.edu.mq.comp.assembly.smt

import au.edu.mq.comp.assembly.smt.Syntax.AnyQId

import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._
import org.bitbucket.franck44.scalasmt.typedterms.TypedTerm
import org.bitbucket.inkytonik.kiama.attribution.Attribution
import org.bitbucket.inkytonik.kiama.relation.Tree

object FreeVariables extends Attribution {

  def compute[T <: Term](term: TypedTerm[_, T]): Set[QualifiedId] = compute(term.aTerm)

  def compute[T <: ASTNode](term: T): Set[QualifiedId] = {
    val tree = new Tree[ASTNode, T](term)
    collectFreeVars(tree).apply(term)
  }

  private def filterBound[T <: ASTNode](tree: Tree[ASTNode, T], term: Term, bindings: List[SortedVar]): Set[QualifiedId] = {
    val termVars = collectFreeVars(tree)(term)
    val boundSymbols = bindings.map(_.sMTLIB2Symbol)
    termVars.filter { case AnyQId(SymbolId(symbol)) => !boundSymbols.contains(symbol) }
  }

  private def collectFreeVars[T <: ASTNode](tree: Tree[ASTNode, T]): ASTNode => Set[QualifiedId] = attr {
    case e: QualifiedId => Set(e)

    // SMTLIB standard 2.6, p. 30:
    // x occurs free in (let ((x_1 t_1) ··· (x_n t_n)) t) if ...
    case LetTerm(bindings, body) =>
      val termVars = collectFreeVars(tree)(body)

      // (i)  it occurs free in some t_i (1 ≤ i ≤ n) and the corresponding x_i occurs free in t
      val freeSymbols = termVars.map { case AnyQId(SymbolId(symbol)) => symbol }
      val bindingVars = for {
        VarBinding(xi, ti) <- bindings if freeSymbols.contains(xi)
        x <- collectFreeVars(tree)(ti)
      } yield x

      // (ii) it does not occur in x_1, ..., x_n and occurs free in t
      val boundSymbols = bindings.map(_.sMTLIB2Symbol)
      val freeTermVars = termVars.filter { case AnyQId(SymbolId(symbol)) => !boundSymbols.contains(symbol) }

      bindingVars.toSet ++ freeTermVars

    case ForAllTerm(bindings, body) => filterBound(tree, body, bindings)
    case ExistsTerm(bindings, body) => filterBound(tree, body, bindings)

    //  otherwise take the union on the children
    case other => tree.child(other).flatMap(collectFreeVars(tree)).toSet
  }
}

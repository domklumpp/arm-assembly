package au.edu.mq.comp.assembly

import org.bitbucket.franck44.scalasmt.configurations.SMTLogics.SMTLogics

import scala.util.Try

/**
  * Represents a machine on which instructions can be executed.
  */
trait Machine {
  /**
    * The logical type of locations in this machine (as defined by scalaSMT).
    */
  type Loc

  /**
    * The set of all program variables, given as un-indexed scalaSMT variables.
    */
  val programVariables: TypedSet[ProgramVariable]

  /**
    * An expression denoting the program counter (PC), the location of the executing instruction.
    * The free variables must be contained in @see `programVariables`.
    */
  val pcExpr: Expr[Loc]

  /**
    * Converts a variable-free expression denoting a location to an integer representing that location.
    *
    * @param expr The expression to convert.
    */
  def expressionToLocation(expr: Expr[Loc]): Try[Location]

  /**
    * Converts an location to a constant SMT expression denoting that location.
    *
    * @param location The location value to convert.
    */
  def locationToExpression(location: Location): Try[Expr[Loc]]

  /**
    * The SMT logic over which instruction semantics for this machine must be expressed.
    */
  val smtLogic: SMTLogics
}

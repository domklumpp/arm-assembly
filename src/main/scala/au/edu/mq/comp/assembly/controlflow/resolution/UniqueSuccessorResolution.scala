package au.edu.mq.comp.assembly
package controlflow
package resolution

import au.edu.mq.comp.assembly.controlflow.UniqueSuccessorAnalysis.{NoSuccessors, SingleSuccessor}
import au.edu.mq.comp.assembly.smt.Syntax._

import scala.util.{Failure, Success, Try}

/**
  * Resolves the successor location for instructions with degree 1, i.e., where the successor location is fully determined by the instruction's location.
  *
  * @param analysis An analysis that can determine unique successors.
  */
case class UniqueSuccessorResolution[I1, I2](analysis: UniqueSuccessorAnalysis[I2], automatonBuilder: PredicateAutomatonBuilder[(Location, I2)])
                                            (implicit instructionSet: InstructionSet[I2])
  extends SuccessorResolution[I1, I2] {

  import instructionSet.machine._

  override def resolve(program: Program[I1], alphabet: Set[(Location, I2)], existing: PredicateAutomatonResolver[(Location, I2)])
                      (trace: LabeledTrace[I2], parallel: Seq[Set[(Location, I2)]]): Try[PredicateAutomatonResolver[(Location, I2)]] = {

    require(trace.nonEmpty, "Unique successor resolution not possible for empty trace.")
    val (location, instruction) = trace.last

    analysis.computeUniqueSuccessor(location, instruction) match {
      case SingleSuccessor(successor) =>
        for (expr <- locationToExpression(successor))
          yield buildResolver(trace, Some(successor, expr), parallel, alphabet, existing)
      case NoSuccessors               => Success(buildResolver(trace, None, parallel, alphabet, existing))
      case _                          => Failure(new Exception(s"Instruction ${instruction.print} does not have a unique successor location."))
    }
  }

  /**
    * Builds a resolver that accepts any trace ending in labeledInstruction.
    *
    * @param successor The successor location for the given final instruction, or none if there is none.
    */
  private def buildResolver(trace: LabeledTrace[I2], successor: Option[(Location, Expr[Loc])], parallel: Seq[Set[(Location, I2)]], alphabet: Set[(Location, I2)], existing: PredicateAutomatonResolver[(Location, I2)]): PredicateAutomatonResolver[(Location, I2)] = {
    val finalPredicate = successor
      .map { case (_, l) => pcExpr === l }
      .getOrElse(False())

    val predicates = List.fill(trace.length)(True()) :+ finalPredicate
    val auto = automatonBuilder.buildPredicateAutomaton(trace, predicates)(parallel, alphabet, existing.hasEdge)

    PredicateAutomatonResolver.create(auto, successor.map(_._1).toSet)
  }
}

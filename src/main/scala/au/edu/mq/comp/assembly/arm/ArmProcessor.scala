package au.edu.mq.comp.assembly
package arm

import au.edu.mq.comp.assembly.arm.ConditionFlag.{C, N, V, Z}
import au.edu.mq.comp.assembly.arm.semantics.{IntOps, RegisterOp, WordSize, registerAsVariable}
import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax._
import au.edu.mq.comp.assembly.arm.syntax.PC
import au.edu.mq.comp.assembly.smt.Syntax._

import org.bitbucket.franck44.scalasmt.configurations.SMTLogics
import org.bitbucket.franck44.scalasmt.configurations.SMTLogics.SMTLogics
import org.bitbucket.franck44.scalasmt.theories.{ArrayTerm, BVTerm}

import scala.util.{Failure, Try}

object ArmProcessor extends Machine {
  /**
    * Memory locations are represented by bit vectors of length 32.
    */
  override type Loc = BVTerm

  /**
    * The program variable representing the memory.
    */
  val memory: ProgramVariable[ArrayTerm[BVTerm]] = ArrayBV1("memory", WordSize.Bits32, WordSize.Byte)

  val stack: ProgramVariable[ArrayTerm[BVTerm]] = ArrayBV1("stack", WordSize.Bits32, WordSize.Byte)
  val programMemory: ProgramVariable[ArrayTerm[BVTerm]] = ArrayBV1("programMemory", WordSize.Bits32, WordSize.Byte)

  /**
    * The ARM processor's variables: registers, condition flags, and the memory.
    */
  override val programVariables: TypedSet[ProgramVariable] = TypedSet.empty[ProgramVariable] +
    R1() + R2() + R3() + R4() + R5() + R6() + R7() + R8() + R9() + R10() + R11() + R12() + R13() + R14() + R15() + N + Z + C + V + memory + stack + programMemory

  /**
    * Converts a variable-free bit vector expression representing a memory location to an integer.
    *
    * @param expr The memory location to convert.
    */
  override def expressionToLocation(expr: Expr[BVTerm]): Try[Location] = {
    import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax._

    expr.termDef match {
      case ConstantTerm(BinLit(s))               => Try(BigInt(s, 2))
      case ConstantTerm(HexaLit(s))              => Try(BigInt(s, 16))
      case ConstantTerm(DecBVLit(BVvalue(s), _)) => Try(BigInt(s, 10))
      case _                                     =>
        Failure(new IllegalArgumentException(s"Failed to translate bit vector expression ${expr.show} to location."))
    }
  }

  /**
    * Converts an integer to a bit vector expression representing a memory location, provided it is within the supported range.
    */
  override def locationToExpression(location: Location): Try[Expr[BVTerm]] = Try(BVs(location, 32, unsigned = true))

  /**
    * The offset between the location of the currently executing instruction and the value of the PC register.
    * See extractPC() below for an explanation.
    */
  val pcOffset = 8

  /**
    * An expression representing the current program counter.
    * Utility function for semantic analysis of ARM programs.
    *
    * @return An expression representing the value of the PC register minus 8.
    *         This offset of two 32-bit words is due to the pipeline architecture:
    *         While an instruction at address a is executing, the instruction at a+4 is decoded
    *         and the instruction at a+8 (the value of PC) is fetched.
    */
  override val pcExpr: Expr[BVTerm] = PC().asVar - pcOffset.ui32

  /**
    * The SMT logic used to encode the semantics of ARM instructions.
    */
  override val smtLogic: SMTLogics = SMTLogics.QF_ABV
}

package au.edu.mq.comp.assembly
package smt

import au.edu.mq.comp.assembly.smt.Solving._
import au.edu.mq.comp.assembly.smt.Syntax._

import org.bitbucket.franck44.scalasmt.configurations.SMTLogics.SMTLogics
import org.bitbucket.franck44.scalasmt.configurations.SMTOptions.MODELS
import org.bitbucket.franck44.scalasmt.interpreters.SMTSolver
import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.{Sat, SatResponses, UnKnown, UnSat}

import scala.util.{Failure, Success, Try}

final case class ValueCollector()(implicit smtCtx: SolverContext) {

  require(smtCtx.supportsOption(MODELS), "Solver does not support model generation.")

  /**
    * Computes all values an expression can have in models for the given system of formulas.
    *
    * @param constraints A system of formulas whose models will be examined.
    * @param expr        An expression whose possible values will be found.
    * @param smtLogic    The SMT logic in which the formulas and expressions are formulated.
    * @tparam T The logical type of the expression.
    * @return A set of constant expressions denoting distinct possible values for the given expression.
    */
  def getValues[T](constraints: Set[Formula], expr: Expr[T], smtLogic: SMTLogics): Try[Set[Expr[T]]] = {
    smtCtx.withSolver(smtLogic, MODELS) { implicit solver =>
      for {
        _ <- constraints.tryMapAll(|=)
        r <- getAllValues(expr)
      } yield r
    }
  }

  private def getAllValues[T](expr: Expr[T])(implicit solver: SMTSolver): Try[Set[Expr[T]]] = {
    for {
      s <- checkSat()
      r <- getAllValues(s, expr)
    } yield r
  }

  private def getAllValues[T](sat: SatResponses, expr: Expr[T])(implicit solver: SMTSolver): Try[Set[Expr[T]]] = sat match {
    case Sat() =>
      for {
        value <- getTypedValue(expr) // retrieve the new value
        _ <- |=(expr =/= value) // make sure recursion does not find the same value again
        successors <- getAllValues(expr) // recursively find other values
      } yield successors + value

    case UnSat()   => Success(Set.empty) // all values have already been found
    case UnKnown() => Failure(new Exception("Satisfiability was unknown"))
  }
}
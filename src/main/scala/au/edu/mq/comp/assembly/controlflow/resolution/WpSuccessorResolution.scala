package au.edu.mq.comp.assembly
package controlflow
package resolution

import au.edu.mq.comp.assembly.smt.Syntax._
import au.edu.mq.comp.assembly.smt.{NoSimplifier, Simplifier, TraceEncoder, ValueCollector}

import scala.util.Try

/**
  * Resolves successors using an SMT solver and computation of weakest preconditions.
  *
  * @param valueCollector   Used to collect possible location values after execution of a trace.
  * @param traceEncoder     Used to encode a trace as SMT formulas.
  * @param automatonBuilder Used to build a resolver automaton from the computed weakest preconditions.
  * @param simplifier       An optional simplifier used to post-process the computed preconditions.
  * @tparam I1 The type of instructions in the underlying program.
  * @tparam I2 The type of concrete instructions in a trace.
  */
case class WpSuccessorResolution[I1, I2](
                                          valueCollector: ValueCollector,
                                          traceEncoder: TraceEncoder[I1, (Location, I2)],
                                          automatonBuilder: PredicateAutomatonBuilder[(Location, I2)],
                                          simplifier: Simplifier = NoSimplifier
                                        )
                                        (implicit instructionSet: InstructionSet[I2])
  extends SuccessorResolution[I1, I2] with Logging {

  import instructionSet.machine.{Loc, expressionToLocation, pcExpr, smtLogic}

  override def resolve(program: Program[I1], alphabet: Set[(Location, I2)], existing: PredicateAutomatonResolver[(Location, I2)])
                      (trace: LabeledTrace[I2], parallel: Seq[Set[(Location, I2)]]): Try[PredicateAutomatonResolver[(Location, I2)]] = {

    require(trace.nonEmpty, "Successor computation not possible for empty trace.")
    logInstructions("trace:", trace)

    for {
      // Encode trace as formulas.
      encoding <- traceEncoder.encode(program, trace, smtLogic)

      _ = logFormulas("initial state:", encoding.initial.toSeq)
      _ = logFormulas("trace encoding:", encoding.traceFormulas)

      // Compute successor locations.
      finalLocation = encoding.states.last.substitute(pcExpr)
      constants <- valueCollector.getValues(encoding.formulas, finalLocation, smtLogic)
      successors <- constants.tryMapAll(expressionToLocation)

      _ = logger.debug(s"found successors: { ${successors.map(_.toString(16)).mkString(", ")} }")

      // Compute weakest (or inductive) preconditions.
      relevanceFilter = (p: StatePredicate, i: Int) => encoding.conditions(i).contains(encoding.states(i).substitute(p)) // Filter used to exclude definedness predicates from preconditions.
      predicates = computePreconditions(trace, finalPredicate(constants), relevanceFilter)

      _ = logFormulas("weakest preconditions:", predicates)

      // Build resolver automaton.
      automaton = automatonBuilder.buildPredicateAutomaton(trace, predicates)(parallel, alphabet, existing.hasEdge)

    } yield PredicateAutomatonResolver.create(automaton, successors)
  }

  /**
    * Computes the final predicate from which weakest preconditions are then computed.
    *
    * @param successors The set of constant expressions denoting the successors.
    */
  private def finalPredicate(successors: Set[Expr[Loc]]): StatePredicate =
    successors.map(pcExpr === _).disjunction

  /**
    * Computes for each instruction the weakest precondition to reach the final predicate
    * by executing the remainder of the trace.
    * If useDependencyProjection is true, the inductive precondition is computed instead.
    *
    * @param trace           The trace.
    * @param predicate       The state predicate after execution of the entire trace.
    * @param relevanceFilter If the inductive precondition is computed instead of the weakest precondition,
    *                        a predicate indicating whether a given definedness predicate at a given (0-based) index is relevant or not.
    * @return A sequence of state predicates of length `trace.size + 1` and ending in predicate.
    */
  private def computePreconditions[I: InstructionSet](trace: Seq[I], predicate: StatePredicate, relevanceFilter: (StatePredicate, Int) => Boolean): List[StatePredicate] = {
    trace.foldRight(List(predicate)) { case (instruction, post :: rest) =>
      val semantics = instruction.semantics

      val index = trace.length - rest.length - 1
      val premises = semantics.isDefined.filter(relevanceFilter(_, index))

      val pre = simplifier.simplify(premises.conjunction implies let(semantics.update)(post), smtLogic)
      pre :: post :: rest
    }
  }
}

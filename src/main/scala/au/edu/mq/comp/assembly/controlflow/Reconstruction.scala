package au.edu.mq.comp.assembly
package controlflow

import au.edu.mq.comp.assembly.controlflow.UniqueSuccessorAnalysis.{NoSuccessors, SingleSuccessor, Unknown}
import au.edu.mq.comp.assembly.controlflow.resolution.SuccessorResolution

import org.bitbucket.franck44.automat.auto.NFA
import org.bitbucket.franck44.automat.util.DFSVisitor

import scala.util.{Success, Try}

/**
  * Reconstructs programs' control flow.
  *
  * @param resolution     The strategy used to compute possible successor locations.
  * @param uniqueAnalysis An analysis that can determine unique successors for some instructions.
  * @param concretize     A function that translates from a general instruction to a set of concrete instructions.
  * @tparam I1 The type of (general) instructions in a program.
  * @tparam I2 The type of concrete instructions in the control flow graph.
  */
case class Reconstruction[I1, I2: InstructionSet](resolution: SuccessorResolution[I1, I2], uniqueAnalysis: UniqueSuccessorAnalysis[I2], concretize: I1 => Set[I2]) extends Logging {
  /**
    * Reconstructs the control flow of a program.
    *
    * @param program The program to be analysed.
    * @return A ControlFlowGraph describing the program.
    */
  def reconstruct(program: Program[I1]): Try[ControlFlowGraph[I1, I2]] = {
    val alphabet = for {
      (l, i) <- program.labeledInstructions
      c <- concretize(i)
    } yield (l, c)

    for {
      initialResolver <- PredicateAutomatonResolver.epsilon[(Location, I2)](program.initialLocation)
      _ = logResolver("initial resolver:", initialResolver)

      _ = logger.info("computing resolver")
      _ = Statistics.startMeasure("resolver")
      finalResolver <- tryFixedPoint(initialResolver)(resolveUnknown(program, alphabet, _))
      _ = Statistics.stopMeasure("resolver")
      _ = logPredicateResolver("final unminimized resolver:", finalResolver)
      _ = Statistics.inspectResolver("unminimized", finalResolver)

      _ = logger.info("minimizing resolver")
      _ = Statistics.startMeasure("minimize")
      minResolver = finalResolver.minimize
      _ = Statistics.stopMeasure("minimize")
      _ = logResolver("final minimized resolver:", minResolver)
      _ = Statistics.inspectResolver("minimized", minResolver)

      _ = logger.info("computation complete")
    } yield ResolverCfg(program, minResolver, uniqueAnalysis, concretize)
  }

  /**
    * Resolves successors for an unresolved state, if any.
    *
    * @param program  The analysed program.
    * @param resolver The existing resolver.
    * @return An update of the original resolver that also resolves a previously unresolved state.
    *         If no such state existed, the original resolver.
    */
  private def resolveUnknown(program: Program[I1], alphabet: Set[(Location, I2)], resolver: PredicateAutomatonResolver[(Location, I2)]): Try[PredicateAutomatonResolver[(Location, I2)]] = {
    logPredicateResolver("current resolver:", resolver)

    val cfg = ResolverCfg(program, resolver, uniqueAnalysis, concretize)
    logCFG("current CFG:", cfg)

    cfg.unresolvedState match {
      case Some(node) =>
        val (_, location) = node
        val instructions = cfg.unresolvedInstructions(node).map((location, _))
        logger.debug(s"Resolving CFG node $node for { ${instructions.map(_.print).mkString(", ")} }")
        Statistics.countResolution()

        for (nodeResolver <- resolveNode(program, alphabet, cfg)(node))
          yield resolver + nodeResolver

      case None => Success(resolver)
    }
  }

  /**
    * Recursively resolves all traces for a given CFG node.
    *
    * @param program  The program being reconstructed.
    * @param cfg      The current partial CFG.
    * @param resolver A recursively accumulated resolver, initially empty.
    * @param target   The target node being resolved.
    * @return A resolver that covers all traces to this node.
    */
  private def resolveNode(program: Program[I1], alphabet: Set[(Location, I2)], cfg: ResolverCfg[I1, I2], resolver: PredicateAutomatonResolver[(Location, I2)] = PredicateAutomatonResolver.empty)(target: cfg.State): Try[PredicateAutomatonResolver[(Location, I2)]] = {
    findTrace(cfg, resolver)(target) match {
      case Some((trace, parallel)) =>
        logInstructions("trace found:", trace)
        for {
          newResolver <- resolution.resolve(program, alphabet, resolver)(trace, parallel)
          _ = Statistics.countRefinement()
          _ = logPredicateResolver("trace resolver:", newResolver)
          finalResolver <- resolveNode(program, alphabet, cfg, resolver + newResolver)(target)
        } yield finalResolver

      case None => Success(resolver)
    }
  }

  /**
    * Finds an unresolved trace in the given control flow graph that ends up in the given state.
    *
    * @param cfg      The control flow graph to analyse.
    * @param resolver The resolver to check if a trace is resolved or not.
    * @param target   The accepting state of the CFG.
    * @return An unresolved trace, whose last state in the CFG is the given target state;
    *         along with the instructions of its companion traces, i.e., traces that are accepted by the same state sequence.
    */
  private def findTrace(cfg: ResolverCfg[I1, I2], resolver: Resolver[(Location, I2)])(target: cfg.State): Option[(LabeledTrace[I2], Seq[Set[(Location, I2)]])] = {

    val (_, location) = target
    val instructions = cfg.unresolvedInstructions(target).map((location, _))

    val result = cfg.dfs(UnresolvedTraceVisitor[cfg.State, I2, resolver.State](target, cfg.location, resolver, resolver.dfa.getInit, instructions))

    if (result.accepted) {
      // Compute the sets of parallel instructions of the unresolved trace.
      val parallel = result.states.init
        .zip(result.states.tail)
        .map { case (v1, v2) => cfg.successors(v1).collect { case (i, `v2`) => (cfg.location(v1), i) } } :+ instructions

      // Exclude the instructions of the trace itself.
      val strictlyParallel = parallel
        .zip(result.trace)
        .map { case (parallelInstructions, traceInstruction) => parallelInstructions - traceInstruction }

      Some(result.trace, strictlyParallel)
    }
    else
      None
  }
}

/**
  * The control flow graph given by a resolver.
  *
  * @param program        The program whose control flow this instance describes.
  * @param resolver       The (possibly incomplete) resolver for the program.
  * @param uniqueAnalysis An analysis that can determine unique successors for some instructions.
  * @param concretize     A function that translates from a general instruction to a set of concrete instructions.
  * @tparam I1 The type of instructions in the program.
  * @tparam I2 The type of concrete instructions in the control flow graph.
  */
private case class ResolverCfg[I1, I2: InstructionSet](program: Program[I1], resolver: Resolver[(Location, I2)], uniqueAnalysis: UniqueSuccessorAnalysis[I2], concretize: I1 => Set[I2]) extends ControlFlowGraph[I1, I2] with Logging {
  override type State = (resolver.State, Location)

  val initialState: State = (resolver.dfa.getInit, program.initialLocation)

  override lazy val states: Set[State] = dfs(new StateCollectorVisitor[State, I2]()).states

  override lazy val initialStates: Set[State] = Set(initialState)

  override def location(state: State): Location = state._2

  override def successors(state: State): Set[(I2, State)] = memoSuccessors(state)

  def dfs[V <: DFSVisitor[State, I2, V]](initialVisitor: V): V = dfs(initialVisitor, initialState)

  private val memoSuccessors = memoize { state: State =>
    logger.debug(s"Computing successors of $state")
    val (q1, l1) = state
    if (program.containsLocation(l1)) {
      for {
        instruction <- concretize(program.instructionAt(l1))
        q2 = resolver.dfa.succ(q1, (l1, instruction))

        resolved = resolver.label(q2)
        unique = uniqueAnalysis.computeUniqueSuccessor(l1, instruction)

        _ = logger.debug(s"examining ${instruction.print} with successor state $q2 (label $resolved) and unique-succ $unique")

        l2 <- (unique, resolved) match {
          case (SingleSuccessor(loc), _) => Set(loc)
          case (NoSuccessors, _)         => Set.empty[Location]
          case (_, Some(s))              => s
          case (_, None)                 => Set.empty[Location]
        }
      } yield (instruction, (q2, l2))
    }
    else {
      logger.error(s"Encountered unknown location ${l1.toString(16)}. No successors.")
      Set.empty[(I2, State)]
    }
  }

  /**
    * An unresolved state in the resolver, if one exists.
    */
  lazy val unresolvedState: Option[State] = dfs(new StateFinderVisitor[State, I2](isUnresolved)).result

  /**
    * Checks if the given state is unresolved.
    */
  private def isUnresolved(state: State): Boolean = unresolvedInstructions(state).nonEmpty

  def unresolvedInstructions(state: State): Set[I2] = memoUnresolvedInstructions(state)

  private val memoUnresolvedInstructions = memoize { state: State =>


    logger.debug(s"computing unresolved instructions of $state")
    val (q1, l1) = state
    if (program.containsLocation(l1)) {
      for {
        instruction <- concretize(program.instructionAt(l1))
        q2 = resolver.dfa.succ(q1, (l1, instruction))

        resolved = resolver.label(q2)
        unique = uniqueAnalysis.computeUniqueSuccessor(l1, instruction)

        _ = logger.debug(s"examining ${instruction.print} with successor state $q2 (label $resolved) and unique-succ $unique")

        if resolved.isEmpty && unique == Unknown
      } yield instruction
    } else {
      logger.error(s"Encountered unknown location ${l1.toString(16)}. No unresolved instructions.")
      Set.empty[I2]
    }
  }
}

private case class StateCollectorVisitor[S, I](states: Set[S] = Set.empty[S]) extends DFSVisitor[S, I, StateCollectorVisitor[S, I]] {
  override def discoverState(s: S, discoveryTime: Map[S, Int], parent: Map[S, (S, I)]): StateCollectorVisitor[S, I] =
    copy(states = states + s)
}

private case class StateFinderVisitor[S, I](predicate: S => Boolean, result: Option[S] = None) extends DFSVisitor[S, I, StateFinderVisitor[S, I]] {
  override def abortSearch: Boolean = result.isDefined

  override def discoverState(s: S, discoveryTime: Map[S, Int], parent: Map[S, (S, I)]): StateFinderVisitor[S, I] = {
    if (predicate(s))
      copy(result = Some(s))
    else
      this
  }
}

/**
  * Used to find unresolved traces in a CFG.
  *
  * @param target        A target node which is not entirely resolved.
  * @param location      A function mapping nodes to locations.
  * @param resolver      A resolver to check whether or not a trace is resolved.
  * @param resolverState The current state of the resolver.
  * @param instructions  The set of final outgoing instructions of the target node.
  * @param revTrace      The (internally) accumulated trace.
  * @param accepted      Whether or not an unresolved trace was found.
  * @tparam S The type of CFG states.
  * @tparam I The type of (concrete) CFG instructions.
  * @tparam R The type of resolver states.
  */
private case class UnresolvedTraceVisitor[S, I, R](
                                                    target: S,
                                                    location: S => Location,
                                                    resolver: Resolver[(Location, I)] {type State = R},
                                                    resolverState: R,
                                                    instructions: Set[(Location, I)],
                                                    private val revTrace: List[(S, R, (Location, I))] = Nil,
                                                    accepted: Boolean = false)
  extends DFSVisitor[S, I, UnresolvedTraceVisitor[S, I, R]] {

  // Accumulate the trace, and make parallel movements in the resolver.
  override def treeEdge(s: S, instruction: I, t: S, discoveryTime: Map[S, Int], parent: Map[S, (S, I)]): UnresolvedTraceVisitor[S, I, R] = {
    val label = (location(s), instruction)
    val nextState = resolver.dfa.succ(resolverState, label)
    copy(revTrace = (s, resolverState, label) :: revTrace, resolverState = nextState)
  }

  // If the target state was discovered, check for unresolved instructions. If one exists, stop the search.
  override def discoverState(s: S, discoveryTime: Map[S, Int], parent: Map[S, (S, I)]): UnresolvedTraceVisitor[S, I, R] = {
    lazy val unresolved = instructions
      .map { i => (i, resolver.dfa.succ(resolverState, i)) }
      .filterNot { case (_, q) => resolver.dfa.isFinal(q) }

    if (target == s && unresolved.nonEmpty) {
      val (instruction, nextState) = unresolved.head
      copy(revTrace = (target, nextState, instruction) :: revTrace, accepted = true)
    }
    else
      this
  }

  override def finishState(s: S, discoveryTime: Map[S, Int], parent: Map[S, (S, I)]): UnresolvedTraceVisitor[S, I, R] =
    revTrace match {
      case (_, prevState, _) :: tailTrace => copy(revTrace = tailTrace, resolverState = prevState)
      case Nil                            => this
    }

  override def abortSearch: Boolean = accepted

  lazy val (states, _, trace): (List[S], List[R], List[(Location, I)]) = revTrace.reverse.unzip3
}
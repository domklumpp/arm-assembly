package au.edu.mq.comp.assembly

import au.edu.mq.comp.assembly.smt.Syntax._

package object semantics {

  // A semantics definition assigns each instruction in I a semantics.
  type SemanticsDefinition[I] = I => Semantics

  type StateUpdate = TypedMap[ProgramVariable, Expr]

  // A semantics is a pair of a definedness predicate and a state update.
  trait Semantics {
    val isDefined: Set[StatePredicate]
    val update: StateUpdate
  }

  object Semantics {
    val undefined: Semantics = new Semantics {
      override val isDefined: Set[StatePredicate] = Set(False())
      override val update: StateUpdate = TypedMap.empty
    }
  }

  implicit class FormulaOp(formula: Formula) {

    import au.edu.mq.comp.assembly.TypedMap.Void

    def ite(thenUpdate: StateUpdate, elseUpdate: StateUpdate): StateUpdate = {
      if (formula == True())
        thenUpdate
      else if (formula == False())
        elseUpdate
      else
        new StateUpdate {
          override def apply[T](v: ProgramVariable[T]): Expr[T] = formula.ite(thenUpdate.getOrElse(v, v), elseUpdate.getOrElse(v, v))

          override def iterateKeys(fct: TypedFunction[ProgramVariable, Void]): Unit = {
            thenUpdate.iterateKeys(fct)
            elseUpdate.iterateKeys(fct)
          }
        }
    }
  }
}
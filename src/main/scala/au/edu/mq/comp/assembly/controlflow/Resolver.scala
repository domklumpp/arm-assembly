package au.edu.mq.comp.assembly
package controlflow

import org.bitbucket.franck44.automat.auto.{DetAuto, NFA}

/**
  * An automaton that resolves successor locations, given a trace.
  *
  * @tparam I The type of instructions in a trace.
  */
trait Resolver[I] extends Logging {
  /**
    * The type of automaton states.
    */
  type State

  /**
    * The resolver's underlying DFA.
    *
    * @return
    */
  def dfa: DetAuto[State, I]

  /**
    * Returns the set of successor locations a state is labeled with, if any.
    */
  def label(q: State): Option[Set[Location]]

  /**
    * Returns the state after reading a trace and its label, if any.
    */
  def apply(trace: Seq[I]): (State, Option[Set[Location]]) = {
    val state = dfa.succ(dfa.getInit, trace)
    (state, label(state))
  }

  /**
    * Returns a minimized resolver that gives the same labels for all traces.
    */
  def minimize: Resolver[I]
}

import org.bitbucket.franck44.scalasmt.parser.SMTLIB2Syntax.Term

/**
  * A resolver whose underlying DFA is given as an NFA with predicates as states.
  *
  * @param nfa      The predicate automaton.
  * @param labeling A map from NFA states to sets of successor locations labeling them.
  * @tparam I The type of instructions in a trace.
  */
final case class PredicateAutomatonResolver[I](
                                                nfa: NFA[Term, I],
                                                labeling: Map[Term, Set[Location]]
                                              ) extends Resolver[I] {

  import org.bitbucket.franck44.automat.edge.{DiEdge, LabDiEdge}

  // not currently supported
  require(nfa.sinkReject.isEmpty)
  require(nfa.sinkAccept.isEmpty)

  // Resolver implementation

  override type State = Set[Term]

  override def dfa: DetAuto[State, I] = nfa

  override def label(q: State): Option[Set[Location]] = {
    val labels = q.flatMap(labeling.get)
    if (labels.isEmpty)
      None
    else
      Some(labels.reduce(_ & _))
  }

  override def minimize: Resolver[I] = {
    import org.bitbucket.franck44.automat.util.Minimiser.{getLanguageEquivStates, quotient}
    import org.bitbucket.franck44.automat.util.{Determiniser, Partition}

    // Replace predicate states by integers for faster determinisation and minimisation.
    // Integer equality and hashCode are much faster.
    val predicates = nfa.states.toList
    val predicateIndex = predicates.zipWithIndex.toMap
    val intNFA = NFA(
      nfa.getInit.map(predicateIndex),
      nfa.transitions.map { case LabDiEdge(DiEdge(src, tgt), lab) => LabDiEdge(DiEdge(predicateIndex(src), predicateIndex(tgt)), lab) },
      nfa.accepting.map(predicateIndex)
    )
    logger.info(s"${intNFA.states.size} states and ${intNFA.transitions.size} edges")

    // Determinise the NFA before minimisation. Record the labels for each state.
    logger.info("determinising...")
    val (detNFA, stateLabelMap) = Determiniser.toDetNFA(intNFA, (q: Set[Int]) => label(q.map(predicates)))
    logger.info("determinisation complete.")

    // Map labels (including "None" if present) to integers representing them.
    val labelIndexMap = stateLabelMap.values.toSet.zipWithIndex.toMap

    // Map states to the integers representing their labels.
    // This serves as initial partition for the minimisation.
    val stateIndexMap = stateLabelMap.mapValues(labelIndexMap)

    //  Compute equivalence classes.
    logger.info("computing equivalence classes")
    val eqvClasses = getLanguageEquivStates(detNFA)(Partition(stateIndexMap))
    logger.info("equivalence classes computed")

    // Minimise DFA using the language equivalent states.
    logger.info("computing quotient")
    val minDFA = quotient(detNFA, eqvClasses)
    logger.info("minimization complete")

    // By reversing, we get a map from integers (representing the equivalence classes) to a representative state,
    // an arbitrary member of the equivalence class.
    val classRepresentativeMap = eqvClasses.p.map { case (repr, cl) => (cl, repr) }

    new Resolver[I] {
      override type State = Set[Int]

      override def minimize: Resolver[I] = this

      override def dfa: DetAuto[Set[Int], I] = minDFA

      override def label(q: Set[Int]): Option[Set[Location]] = {
        val labels = for {
          nfaState <- q
          representative <- classRepresentativeMap.get(nfaState)
          optLabel <- stateLabelMap.get(representative)
          label <- optLabel
        } yield label

        if (labels.isEmpty)
          None
        else
          Some(labels.reduce(_ & _))
      }
    }
  }

  // Extended functionality

  /**
    * Tests if the predicate NFA has a given edge.
    *
    * @param src         The source state / predicate.
    * @param instruction The instruction the edge is labeled with.
    * @param tgt         The target state / predicate.
    */
  def hasEdge(src: Term, instruction: I, tgt: Term): Boolean = nfa.transitions.contains(LabDiEdge(DiEdge(src, tgt), instruction))

  /**
    * Combines two predicate automata.
    * The returned automaton accepts at least the union of the two languages, usually more.
    */
  def +(other: PredicateAutomatonResolver[I]): PredicateAutomatonResolver[I] = {
    val combined =
      (labeling.keySet ++ other.labeling.keySet)
        .map { k => k -> (labeling.get(k), other.labeling.get(k)) }
        .toMap
        .mapValues {
          case (Some(a), Some(b)) => a & b
          case (Some(a), _)       => a
          case (_, Some(b))       => b
          case _                  => ??? // can never happen, as at least one map contains the key
        }

    PredicateAutomatonResolver(
      NFA(nfa.getInit ++ other.nfa.getInit, nfa.transitions ++ other.nfa.transitions, nfa.accepting ++ other.nfa.accepting),
      combined
    )
  }
}

object PredicateAutomatonResolver {

  import au.edu.mq.comp.assembly.smt.Syntax._

  import scala.util.Try

  /**
    * Creates an empty resolver, that assigns no locations to any trace.
    */
  def empty[I]: PredicateAutomatonResolver[I] = PredicateAutomatonResolver[I](NFA(Set.empty, Set.empty, Set.empty), Map.empty)

  /**
    * Creates a resolver that only accepts the empty trace and assigns the given location to it.
    */
  def epsilon[I](location: Location)(implicit is: InstructionSet[I]): Try[PredicateAutomatonResolver[I]] = {
    for {
      expr <- is.machine.locationToExpression(location)
      predicate = (is.machine.pcExpr === expr).aTerm
    } yield PredicateAutomatonResolver(
      NFA(Set(predicate), Set.empty, Set(predicate)),
      Map(predicate -> Set(location))
    )
  }

  /**
    * Creates a resolver from the given NFA that labels all accepting states with the given set of locations.
    */
  def create[I](nfa: NFA[Term, I], label: Set[Location]): PredicateAutomatonResolver[I] =
    PredicateAutomatonResolver(
      nfa,
      nfa.accepting.map((_, label)).toMap
    )
}
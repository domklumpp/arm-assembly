package au.edu.mq.comp.assembly.arm.translation

import au.edu.mq.comp.assembly.controlflow.{BasicBlock, BasicBlocks}

import org.bitbucket.inkytonik.kiama.attribution.Attribution

final case class SSAVariables[V, I](variables: Set[String], blocks: BasicBlocks[V, I], defines: (I, String) => Boolean) extends Attribution {
  private val blockOf: Map[V, (BasicBlock[V, I], Int)] = (
    for {
      block <- blocks.blocks
      (v, i) <- block.nodes.zipWithIndex
    } yield v -> (block, i)
    ).toMap

  lazy val vars: V => Map[String, String] = circular(Map.empty[String, String]) { v: V =>
    val (block, index) = blockOf(v)
    if (index == 0)
      blockIn(block)
    else {
      val instruction = block.instructions(index - 1)
      val newDefs = variables.collect { case x if defines(instruction, x) => (x, nodeVariable(v, x)) }

      val previous = block.nodes(index - 1)
      vars(previous) ++ newDefs
    }
  }

  lazy val blockIn: BasicBlock[V, I] => Map[String, String] = circular(Map.empty[String, String]) { b: BasicBlock[V, I] =>
    if (blocks.entryBlocks.contains(b))
      variables.map { x => (x, nodeVariable(b.head, x)) }.toMap
    else {
      blocks.predecessors(b)
        .flatMap { case (_, pre) => blockOut(pre) }
        .groupBy(_._1).mapValues(_.map(_._2))
        .collect {
          case (x, instances) if instances.size == 1 => (x, instances.head)
          case (x, instances) if instances.size != 1 => (x, nodeVariable(b.head, x))
        }
    }
  }

  lazy val blockInit: BasicBlock[V, I] => Map[String, Set[(BasicBlock[V, I], String)]] = attr { b: BasicBlock[V, I] =>
    val inits = for {
      (_, pre) <- blocks.predecessors(b)
      (x, name) <- blockOut(pre)
    } yield (x, (pre, name))
    inits.groupBy(_._1).mapValues(_.map(_._2))
  }

  lazy val blockOut: BasicBlock[V, I] => Map[String, String] = circular(Map.empty[String, String]) { b: BasicBlock[V, I] => vars(b.nodes.last) }

  private val nodeIndex: Map[V, Int] = blocks.blocks.flatMap(_.nodes).zipWithIndex.toMap

  private def nodeVariable(v: V, x: String): String = s"$x${nodeIndex(v)}"
}

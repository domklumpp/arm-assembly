package au.edu.mq.comp.assembly
package arm.semantics

import au.edu.mq.comp.assembly.arm.ArmProcessor
import au.edu.mq.comp.assembly.arm.syntax.ARMSyntax._
import au.edu.mq.comp.assembly.arm.syntax.{PC, SP}
import au.edu.mq.comp.assembly.semantics.{Semantics, SemanticsDefinition, StateUpdate}

import org.bitbucket.franck44.scalasmt.theories.{ArrayTerm, BVTerm}

/**
  * A semantics for AArch32-based instruction sets that distinguishes between different memory regions -- stack, program memory and heap.
  */
object MemoryRegionSemantics extends SemanticsDefinition[Instruction] {
  def apply(instruction: Instruction): Semantics = instruction match {
    case BatchMemoryInstruction(_, SP(), _, _) | MemoryInstruction(_, _, NoOffset(SP()) | UnIndexed(SP(), _) | PostIndexed(SP(), _) | PreIndexed(SP(), _)) =>
      substituteMemoryAccesses(InstructionSemantics(instruction), ArmProcessor.stack)

    case BatchMemoryInstruction(_, PC(), _, _) | MemoryInstruction(_, _, NoOffset(PC()) | UnIndexed(PC(), _) | PostIndexed(PC(), _) | PreIndexed(PC(), _)) =>
      substituteMemoryAccesses(InstructionSemantics(instruction), ArmProcessor.programMemory)

    case _ => InstructionSemantics(instruction)
  }

  private def substituteMemoryAccesses(semantics: Semantics, memoryVariable: ProgramVariable[ArrayTerm[BVTerm]]): Semantics = {
    val fct = substituteMemory(memoryVariable)
    val mappedCondition = semantics.isDefined.map(fct.apply)
    val mappedUpdate = semantics.update.mapValues(fct)

    val finalUpdate = if (mappedUpdate.keySet.contains(ArmProcessor.memory))
      mappedUpdate.filter(isNotMemory) ++ (memoryVariable |-> mappedUpdate(ArmProcessor.memory))
    else
      mappedUpdate

    new Semantics {
      override val isDefined: Set[StatePredicate] = mappedCondition
      override val update: StateUpdate = finalUpdate
    }
  }

  private object isNotMemory extends TypedMap.Callback[ProgramVariable, Expr, Boolean] {
    override def apply[T](k: ProgramVariable[T], v: Expr[T]): Boolean = k != ArmProcessor.memory
  }

  private case class substituteMemory(memoryVariable: ProgramVariable[ArrayTerm[BVTerm]]) extends TypedFunction[Expr, Expr] {
    override def apply[T](expr: Expr[T]): Expr[T] = expr.substitute(ArmProcessor.memory, memoryVariable)
  }

}

package au.edu.mq.comp.assembly.arm

import au.edu.mq.comp.assembly.smt.Syntax._

/**
  * Program variables representing the comparison condition code flags.
  */
object ConditionFlag {
  val N = Bools("n")
  val Z = Bools("z")
  val C = Bools("c")
  val V = Bools("v")
}
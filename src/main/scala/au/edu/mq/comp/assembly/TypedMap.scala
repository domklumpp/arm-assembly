package au.edu.mq.comp.assembly

trait TypedMap[K[_], V[_]] extends TypedFunction[K, V] {

  import TypedMap.Void

  /**
    * Iterates over all keys in the map and invokes a callback on them.
    * May invoke the callback several times with the same key.
    *
    * @param fct The callback to invoke.
    */
  def iterateKeys(fct: TypedFunction[K, Void]): Unit


  /** ***** Predefined Methods ***********************************************
    * *************************************************************************/

  lazy val keys: TypedSet[K] ={
    var set = TypedSet.empty[K]
    iterateKeys(new TypedFunction[K, Void] {
      override def apply[T](k: K[T]): Unit = set += k
    })
    set
  }

  lazy val values: TypedSet[V] = {
    var set = TypedSet.empty[V]
    iterateKeys(new TypedFunction[K, Void] {
      override def apply[T](k: K[T]): Unit = set += TypedMap.this(k)
    })
    set
  }

  /**
    * The set of all keys in the map.
    */
  lazy val keySet: Set[K[_]] = keys.toSet

  /**
    * Iterates over all key/value pairs in the map and invokes the callback on them.
    * May invoke the callback several times with the same key/value pair.
    *
    * @param fct The callback to invoke.
    */
  def iterate(fct: TypedMap.Callback[K, V, Unit]): Unit = {
    iterateKeys(new TypedFunction[K, Void] {
      override def apply[T](k: K[T]): Void[T] = fct(k, TypedMap.this (k))
    })
  }

  /**
    * Iterates over all key/value pairs in the map and collects the return values of a callback function invoked on them.
    *
    * @param fct The callback to invoke.
    * @tparam A The callback's return type.
    * @return The set of all return values.
    */
  def map[A](fct: TypedMap.Callback[K, V, A]): Set[A] = {
    var set = Set.empty[A]
    iterate(new TypedMap.Callback[K, V, Unit] {
      override def apply[T](k: K[T], v: V[T]): Unit = set += fct(k, v)
    })
    set
  }

  /**
    * Replaces each value with the function's respective return value.
    */
  def mapValues[A[_]](fct: TypedFunction[V, A]): TypedMap[K, A] = {
    val pairs = map(new TypedMap.Callback[K, V, TypedMap[K, A]] {
      override def apply[T](k: K[T], v: V[T]): TypedMap[K, A] = k |-> fct(v)
    })
    if (pairs.isEmpty)
      TypedMap.empty[K, A]
    else
      pairs.reduce(_ ++ _)
  }

  /**
    * Filters the key/value pairs in the map.
    *
    * @param filter A function that returns true for those pairs that should be included in the returned map.
    */
  def filter(filter: TypedMap.Callback[K, V, Boolean]): TypedMap[K, V] = {
    val maps = map(new TypedMap.Callback[K, V, Set[TypedMap[K, V]]] {
      override def apply[T](k: K[T], v: V[T]): Set[TypedMap[K, V]] = if (filter(k, v)) Set(k |-> v) else Set.empty
    }).flatten

    if (maps.isEmpty)
      TypedMap.empty
    else
      maps.reduce(_ ++ _)
  }

  /**
    * Returns true if the map is empty, false otherwise.
    */
  def isEmpty: Boolean = keySet.isEmpty

  /**
    * Checks if a given key is in the map.
    *
    * @param k The key to check for.
    * @tparam T The key's type.
    * @return true if the key is in the map, false otherwise.
    */
  def isDefined[T](k: K[T]): Boolean = keySet.contains(k)

  /**
    * Retrieves a key's value from the map, and falls back to a default value if the key is not found.
    *
    * @param k The key whose value shall be retrieved.
    * @param e The default value in case the key is not found. Note that this value is not stored in the map.
    * @tparam T The key's type.
    */
  def getOrElse[T](k: K[T], e: => V[T]): V[T] = if (isDefined(k)) apply(k) else e

  /**
    * Merges two maps. The right hand side map has precedence if a key is contained in both maps.
    *
    * @param other The map to merge with this map.
    * @return A new map with the keys of both input maps.
    */
  def ++(other: TypedMap[K, V]): TypedMap[K, V] = {
    new TypedMap[K, V] {
      override def apply[T](k: K[T]): V[T] = if (other.isDefined(k)) other(k) else TypedMap.this (k)

      override def iterateKeys(fct: TypedFunction[K, Void]): Unit = {
        TypedMap.this.iterateKeys(fct)
        other.iterateKeys(fct)
      }
    }
  }
}

object TypedMap {
  /**
    * Returns an empty typed map.
    *
    * @tparam K The key type.
    * @tparam V The value type.
    */
  def empty[K[_], V[_]]: TypedMap[K, V] = new TypedMap[K, V] {
    override def apply[T](k: K[T]): V[T] = throw new IllegalStateException()

    override def iterateKeys(fct: TypedFunction[K, Void]): Unit = {}
  }

  /**
    * A type-safe callback type for TypedMap's functions.
    *
    * @tparam K The map's key type.
    * @tparam V The map's value type.
    * @tparam A The return type of the callback.
    */
  trait Callback[K[_], V[_], A] {
    def apply[T](k: K[T], v: V[T]): A
  }

  trait A[T] {
    type λ[S] = T
  }

  type Void[S] = A[Unit]#λ[S]
}
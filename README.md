# Control Flow Reconstruction

Control flow analysis of assembler programs is crucial to apply standard software analysis techniques that rely on the availability of control flow graphs. However, it is notoriously difficult due to the presence of *indirect jumps* to dynamically calculated target addresses. This repo contains a framework for control flow reconstruction from assembler programs, based on *trace abstraction refinement*, and an instantiation of the framework for (a subset of) ARM AArch32 assembler.

The approach this framework is based on is described in the forthcoming conference paper *Construction of Control Flow Graphs for Binary Programs* at TACAS '19 and the forthcoming Master thesis *Automated Control Flow Reconstruction From Assembler Programs* at the University of Augsburg.

## Setup

Compile with sbt 1.2.3 and scala 2.12.7. The following will download and compile the code and show an overview over the available command line parameters.
```bash
> git clone https://domklumpp@bitbucket.org/domklumpp/arm-assembly.git
> cd arm-assembly
> sbt run --help
```

Example programs are located in `src/test/resources/benchmarks`. To compile the C programs, install gcc for ARM. On Ubuntu:
```bash
> apt-get install gcc-arm-none-eabi
```
Then run the compilation script:
```bash
> ./scripts/compile-benchmarks.sh
```
When compiling programs other than the included examples, note that only a subset of AArch32 is supported, and programs with unsupported instructions will cause the tool to fail.

In order to run the tool, you will require an SMT solver. Run one of the following commands to acquire one (on Ubuntu):
```bash
> . ./scripts/install-boolector.sh 3.0.0
> . ./scripts/install-cvc4.sh 1.4
> . ./scripts/install-yices.sh 2.6.0
```

### Docker
Instead of setting up all required resources directly on your machine, you can use docker:
```bash
> git clone https://domklumpp@bitbucket.org/domklumpp/arm-assembly.git
> cd arm-assembly
> docker build -t arm-assembly .
> docker run -it arm-assembly
```
This will drop you in an `sbt` shell, where you can execute the tool:

```bash
> run --help
```

Alternatively, running
```bash
> docker run -it arm-assembly bash
```
will give you a bash shell.

The Ubuntu-based container comes pre-equipped with gcc for ARM, SMT solvers, scala and sbt.

## License
The code is available under the MIT license. See the `LICENSE` file included with this repository.

inThisBuild(List(
  organization := "au.edu.mq.comp",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.12.7"
))

Global / cancelable := true

scalacOptions ++= Seq(
  "-deprecation",
  "-feature",
  "-language:higherKinds",
  "-language:implicitConversions"
)

ThisBuild / resolvers ++= Seq(
  Resolver.sonatypeRepo("releases"),
  Resolver.sonatypeRepo("snapshots")
)

lazy val root = (project in file("."))
  .settings(
    name := "ARM Assembly Language",
    libraryDependencies ++=
      Seq(
        "ch.qos.logback"                      %  "logback-classic"     % "1.2.3",
        "org.bitbucket.franck44.scalasmt"     %% "scalasmt"            % "2.1.1-SNAPSHOT",
        "org.bitbucket.franck44.automat"      %% "automat"             % "1.2.2-SNAPSHOT",
        "org.bitbucket.inkytonik.kiama"       %% "kiama"               % "2.2.0",
        "org.bitbucket.inkytonik.scalallvm"   %% "scalallvm"           % "0.2.0-SNAPSHOT",
        "com.typesafe.scala-logging"          %% "scala-logging"       % "3.7.2",
        "org.rogach"                          %% "scallop"             % "3.1.3",
        "org.scalatest"                       %% "scalatest"           % "3.0.5"               % "test"
      )
  )

// recommended by scalatest <http://www.scalatest.org/user_guide/using_scalatest_with_sbt>
Test / logBuffered := false

// scalaSMT seems to have problems with parallel execution
Test / parallelExecution := false

// enable test summary in file
Test / testOptions += Tests.Argument(TestFrameworks.ScalaTest, "-oDI")

// sbt-rats settings
ratsScalaRepetitionType := Some(ListType)
ratsUseScalaOptions := true
ratsUseScalaPositions := true
ratsDefineASTClasses := true
ratsUseKiama := 2
ratsDefinePrettyPrinter := true
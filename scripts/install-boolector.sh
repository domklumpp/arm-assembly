#!/bin/bash

BOOLECTOR_VERSION=$1
DIR="solvers/boolector/$BOOLECTOR_VERSION"

# create cache directory
mkdir -p git-dependencies
cd git-dependencies

# Clone or update repository
if [ -d "boolector" ]
then
  echo ">>>>> Using cached boolector repository <<<<<"
else
  echo ">>>>> Cloning boolector git repository <<<<<"
  git clone https://github.com/boolector/boolector
fi

cd boolector
git checkout master
git pull --tags
git checkout "$BOOLECTOR_VERSION"

# Patch for (echo): Flush stream after output.
ECHO_FLUSH="  fprintf (parser->outfile, \"%s\", parser->token.start); fflush (parser->outfile);"
ECHO_LINENUMBER=3656
ECHO_LINE=$(sed "${ECHO_LINENUMBER}q;d" src/parser/btorsmt2.c)

if [ "$ECHO_LINE" = "$ECHO_FLUSH" ]
then
  echo ">>>>> boolector (echo)-command was patched previously <<<<<"
else
  echo ">>>>> Patching boolector (echo)-command <<<<<"
  sed "${ECHO_LINENUMBER}s/.*/${ECHO_FLUSH}/" src/parser/btorsmt2.c > src/parser/btorsmt2.c.fix
  mv src/parser/btorsmt2.c.fix src/parser/btorsmt2.c
fi

# Patch for (get-value): Do not print success.
GETVALUE_SUCCESS="      //print_success (parser);"
GETVALUE_LINENUMBER=4123
GETVALUE_LINE=$(sed "${GETVALUE_LINENUMBER}q;d" src/parser/btorsmt2.c)

if [ "$GETVALUE_LINE" = "$GETVALUE_SUCCESS" ]
then
  echo ">>>>> boolector (get-value)-command was patched previously <<<<<"
else
  echo ">>>>> Patching boolector (get-value)-command <<<<<"
  sed "${GETVALUE_LINENUMBER}s#.*#${GETVALUE_SUCCESS}#" src/parser/btorsmt2.c > src/parser/btorsmt2.c.fix
  mv src/parser/btorsmt2.c.fix src/parser/btorsmt2.c
fi

# Check if recompilation necessary or not
COMPILE=false
CURRENT_COMMIT=$(git rev-parse HEAD)
if [ -f ".last-build-commit" ]
then
  LAST_BUILD_COMMIT=$(cat .last-build-commit)
  echo ">>>>> boolector last compiled at $LAST_BUILD_COMMIT <<<<<"
  if [ "$CURRENT_COMMIT" == "$LAST_BUILD_COMMIT" ]
  then
    echo ">>>>> boolector up-to-date <<<<<"
  else
    COMPILE=true
  fi
else
  echo ">>>>> boolector not previously compiled <<<<<"
  COMPILE=true
fi

# Compile if necessary
if [ "$COMPILE" == "true" ]
then
  echo ">>>>> Building boolector $BOOLECTOR_VERSION dependencies <<<<<"
  ./contrib/setup-lingeling.sh
  ./contrib/setup-btor2tools.sh

  echo ">>>>> Building boolector $BOOLECTOR_VERSION <<<<<"
  apt-get update && apt-get install -y cmake
  ./configure.sh && cd build && make && cd ..

  # Update last build commit
  touch ".last-build-commit"
  echo "$CURRENT_COMMIT" > ".last-build-commit"
fi

cd ../..

echo ">>>>> Linking boolector $BOOLECTOR_VERSION <<<<<"
mkdir -p "$DIR"
mv git-dependencies/boolector/build/bin "$DIR"
mv git-dependencies/boolector/build/lib "$DIR"
export PATH="$PATH:$DIR/bin/"

echo ">>>>> Installed boolector <<<<<"
boolector --version
